<?php

namespace App\Exports;

use DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MhsExport implements FromView, ShouldAutoSize
{
    protected $nim;

    public function __construct($nim)
    {
        $this->nim = $nim;
    }

    public function view(): View
    {
        $mhs = DB::table('mahasiswa AS m')
            ->join('kelas AS k', 'm.id_kelas', 'k.id_kelas')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('m.*', 'k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->where('m.nim', $this->nim)
            ->first();

        $absenSql = "
        SELECT matkul, tanggal, SUM(alpha) AS alpha, SUM(ijin) AS ijin, SUM(sakit) AS sakit FROM (
            SELECT matkul, tanggal, COALESCE(SUM(jml_absen), 0) AS alpha, 0 AS ijin, 0 AS sakit
            FROM absen
            WHERE nim = '$this->nim' AND st_absen = 'A'
            GROUP BY tanggal
            UNION
            SELECT matkul, tanggal, 0 AS alpha, COALESCE(SUM(jml_absen), 0) AS ijin, 0 AS sakit
            FROM absen
            WHERE nim = '$this->nim' AND st_absen = 'I'
            GROUP BY tanggal
            UNION
            SELECT matkul, tanggal, 0 AS alpha, 0 AS ijin, COALESCE(SUM(jml_absen), 0) AS sakit
            FROM absen
            WHERE nim = '$this->nim' AND st_absen = 'S'
            GROUP BY tanggal
            ) AS b
            WHERE matkul IS NOT NULL
            GROUP BY tanggal
        ";

        $absen = DB::select(DB::raw($absenSql));

        return view('exports.mhsExport', [
            'mhs' => $mhs,
            'absen' => $absen,
        ]);
    }
}
