<?php

namespace App\Exports;

use DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class RekapAbsenExport implements FromView, ShouldAutoSize, WithStyles, WithColumnWidths
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A5:H5')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A5:H5')->getAlignment()->setVertical('center');
        $sheet->getStyle('A1:C3')->getFont()->setBold('bold');
    }

    public function columnWidths(): array
    {
        return [
            'A' => 4,
            'B' => 12,
            'D' => 7,
            'E' => 7,
            'F' => 7,
            'G' => 7,
        ];
    }

    public function view(): View
    {
        $kls = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->where('k.id_kelas', $this->id)
            ->first();

        $rekapSql = "
        SELECT a.nim, m.nama_mhs, SUM(alpha) AS alpha, SUM(ijin) AS ijin, SUM(sakit) AS sakit, (SUM(alpha)+SUM(ijin)+SUM(sakit)) AS total
        FROM (
            SELECT a.nim, COALESCE(SUM(jml_absen), 0) AS alpha, 0 AS ijin, 0 AS sakit
            FROM kelas AS k
            LEFT JOIN jadwal AS j ON k.id_kelas = j.id_kelas AND j.id_kelas = $this->id
            LEFT JOIN absen AS a ON a.id_jadwal = j.id_jadwal
            WHERE st_absen = 'A'
            GROUP BY a.nim
            UNION
            SELECT a.nim, 0 AS alpha, COALESCE(SUM(jml_absen), 0) AS ijin, 0 AS sakit
            FROM kelas AS k
            LEFT JOIN jadwal AS j ON k.id_kelas = j.id_kelas AND j.id_kelas = $this->id
            LEFT JOIN absen AS a ON a.id_jadwal = j.id_jadwal
            WHERE st_absen = 'I'
            GROUP BY a.nim
            UNION
            SELECT a.nim, 0 AS alpha, 0 AS ijin, COALESCE(SUM(jml_absen), 0) AS sakit
            FROM kelas AS k
            LEFT JOIN jadwal AS j ON k.id_kelas = j.id_kelas AND j.id_kelas = $this->id
            LEFT JOIN absen AS a ON a.id_jadwal = j.id_jadwal
            WHERE st_absen = 'S'
            GROUP BY a.nim
            ) AS a
            JOIN mahasiswa AS m ON a.nim = m.nim
            GROUP BY a.nim
            ";

        $rekap = DB::select(DB::raw($rekapSql));

        return view('exports.rekapExport', [
            'rekap' => $rekap,
            'kls' => $kls,
        ]);
    }
}
