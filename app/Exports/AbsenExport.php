<?php

namespace App\Exports;

use App\Models\ExsportAbsen;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use DB;

class AbsenExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles
{

    public function headings(): array
    {
        return [
            'NIM',
            'NAMA LENGKAP',
            'TANGGAL',
            'MATA KULIAH',
            'KET',
        ];
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return DB::table('absen')->select('nim', 'nama_lengkap', 'tanggal', 'matkul', 'keterangan')->get();
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:G16')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);;
    }
}
