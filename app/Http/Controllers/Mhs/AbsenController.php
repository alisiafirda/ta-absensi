<?php

namespace App\Http\Controllers\Mhs;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class AbsenController extends Controller
{
    public function index()
    {
        $time = date('H:i:s');

        $mhs = DB::table('mahasiswa AS m')
            ->join('kelas AS k', 'm.id_kelas', 'k.id_kelas')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('m.*', 'k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->where('user_id', auth()->user()->mahasiswa_id)
            ->first();

        $sdh = DB::table('absen AS a')
            ->where(['nim' => $mhs->nim, 'tanggal' => date('Y-m-d')])
            ->pluck('id_jadwal');

        $absen = DB::table('mahasiswa AS m')
            ->join('kelas AS k', 'm.id_kelas', 'k.id_kelas')
            ->join('jadwal AS j', 'j.id_kelas', 'k.id_kelas')
            ->join('matkul AS mt', 'mt.kode_matkul', 'j.kode_matkul')
            ->join('jwd_jm AS jw', 'jw.id_jadwal', 'j.id_jadwal')
            ->join('jam AS jm', function ($j) use ($time) {
                $j->on('jw.id_jam', 'jm.id_jam')
                    ->where('jm.mulai', '<=', $time)
                    ->where('jm.selesai', '>=', $time);
            })
            ->where('user_id', auth()->user()->mahasiswa_id)
            ->whereRaw('DAYOFWEEK(CURDATE()) = int_hari')
            ->whereNotIn('j.id_jadwal', $sdh)
            ->first();

        $cekJadwal = DB::table('mahasiswa AS m')
            ->join('kelas AS k', 'm.id_kelas', 'k.id_kelas')
            ->join('jadwal AS j', 'j.id_kelas', 'k.id_kelas')
            ->join('matkul AS mt', 'mt.kode_matkul', 'j.kode_matkul')
            ->join('absen AS a', 'a.id_jadwal', 'j.id_jadwal')
            ->join('jwd_jm AS jw', 'jw.id_jadwal', 'j.id_jadwal')
            ->join('jam AS jm', function ($j) use ($time) {
                $j->on('jw.id_jam', 'jm.id_jam')
                    ->where('jm.mulai', '<=', $time)
                    ->where('jm.selesai', '>=', $time);
            })
            ->where('user_id', auth()->user()->mahasiswa_id)
            ->whereRaw('DAYOFWEEK(CURDATE()) = int_hari')
            ->count();

        return view('mhs.absen.index')->with(['absen' => $absen, 'mhs' => $mhs, 'cek' => $cekJadwal]);
    }

    public function hadir(Request $request)
    {
        $jadwal = DB::table('jadwal')->where('id_jadwal', $request->id_jadwal)->first();

        $jam = DB::table('jwd_jm AS jw')
        ->join('jam AS j', 'jw.id_jam', 'j.id_jam')
        ->select(DB::raw("MIN(j.mulai) AS mulai"))
        ->where('jw.id_jadwal', $jadwal->id_jadwal)
        ->first();

        $jam_mulai = date_create($jam->mulai);
        $jam_hadir = date_create(date('H:i:s'));

        $interval = date_diff($jam_mulai, $jam_hadir);
        $minutes = $interval->format('%H') * 60 + $interval->format('%i');
        $alpha = ceil($minutes / 50);

        $msg = 'Absen Berhasil';

        if ($alpha) {
            DB::table('absen')->insert([
                'id_jadwal' => $request->id_jadwal,
                'nim' => $request->nim,
                'nama_lengkap' => $request->nama_mhs,
                'prodi' => $request->prodi,
                'tanggal' => date('Y-m-d'),
                'matkul' => $request->matkul,
                'st_absen' => 'A',
                'jml_absen' => $alpha,
                'keterangan' => 'Alpha Hadir',
            ]);

            $msg = "$msg, Alpha $alpha";
        }

        DB::table('absen')->insert([
            'id_jadwal' => $request->id_jadwal,
            'nim' => $request->nim,
            'nama_lengkap' => $request->nama_mhs,
            'prodi' => $request->prodi,
            'tanggal' => date('Y-m-d'),
            'matkul' => $request->matkul,
            'st_absen' => 'H',
            'keterangan' => 'Hadir',
        ]);

        return redirect()->route('absensi')->with('status', $msg);
    }

    public function formIzin()
    {
        $time = date('H:i:s');

        $mhs = DB::table('mahasiswa AS m')
            ->join('kelas AS k', 'm.id_kelas', 'k.id_kelas')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('m.*', 'k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->where('user_id', auth()->user()->mahasiswa_id)
            ->first();

        $sdh = DB::table('absen AS a')
            ->where(['nim' => $mhs->nim, 'tanggal' => date('Y-m-d')])
            ->pluck('id_jadwal');

        $absen = DB::table('mahasiswa AS m')
            ->join('kelas AS k', 'm.id_kelas', 'k.id_kelas')
            ->join('jadwal AS j', 'j.id_kelas', 'k.id_kelas')
            ->join('matkul AS mt', 'mt.kode_matkul', 'j.kode_matkul')
            ->join('jwd_jm AS jw', 'jw.id_jadwal', 'j.id_jadwal')
            ->join('jam AS jm', function ($j) use ($time) {
                $j->on('jw.id_jam', 'jm.id_jam')
                    ->where('jm.mulai', '<=', $time)
                    ->where('jm.selesai', '>=', $time);
            })
            ->where('user_id', auth()->user()->mahasiswa_id)
            ->whereRaw('DAYOFWEEK(CURDATE()) = int_hari')
            ->whereNotIn('j.id_jadwal', $sdh)
            ->first();

        return view('mhs.absen.formIzin')->with(['absen' => $absen, 'mhs' => $mhs]);
    }

    public function izin(Request $request)
    {
        $jadwal = DB::table('jadwal')->where('id_jadwal', $request->id_jadwal)->first();

        $jam = DB::table('jwd_jm AS jw')
        ->join('jam AS j', 'jw.id_jam', 'j.id_jam')
        ->select(DB::raw("MIN(j.mulai) AS mulai, MAX(j.selesai) AS selesai"))
        ->where('jw.id_jadwal', $jadwal->id_jadwal)
        ->first();

        $jam_mulai = date_create($jam->mulai);
        $jam_selesai = date_create($jam->selesai);

        $interval = date_diff($jam_mulai, $jam_selesai);
        $minutes = $interval->format('%H') * 60 + $interval->format('%i');
        $jmlAbsen = ceil($minutes / 50);

        $file = $request->bukti;
        $nm_bukti = $request->nim . '-' . $request->id_jadwal . '-' . time() . rand() . '.' . $file->getClientOriginalExtension();

        $path = public_path('bukti/');
        $file->move($path, $nm_bukti);

        DB::table('absen')->insert([
            'id_jadwal' => $request->id_jadwal,
            'nim' => $request->nim,
            'nama_lengkap' => $request->nama_mhs,
            'prodi' => $request->prodi,
            'tanggal' => date('Y-m-d'),
            'matkul' => $request->matkul,
            'st_absen' => $request->st_absen,
            'jml_absen' => $jmlAbsen,
            'bukti' => $nm_bukti,
            'keterangan' => $request->keterangan,
        ]);

        return redirect()->route('absensi')->with('status', 'Izin Berhasil');
    }

    public function dtlAbsen()
    {
        $mahasiswa_id = auth()->user()->mahasiswa_id;

        $mhs = DB::table('mahasiswa')->where('user_id', $mahasiswa_id)->first();
        $nim = $mhs->nim;

        $sql = "
        SELECT a.nim, m.nama_mhs, SUM(alpha) AS alpha, SUM(ijin) AS ijin, SUM(sakit) AS sakit, (SUM(alpha)+SUM(ijin)+SUM(sakit)) AS total
        FROM (
            SELECT COALESCE(a.nim, '$nim') AS nim, COALESCE(SUM(jml_absen), 0) AS alpha, 0 AS ijin, 0 AS sakit
            FROM kelas AS k
            LEFT JOIN jadwal AS j ON k.id_kelas = j.id_kelas
            LEFT JOIN absen AS a ON a.id_jadwal = j.id_jadwal
            WHERE st_absen = 'A' AND a.nim = '$nim'
            UNION
            SELECT COALESCE(a.nim, '$nim') AS nim, 0 AS alpha, COALESCE(SUM(jml_absen), 0) AS ijin, 0 AS sakit
            FROM kelas AS k
            LEFT JOIN jadwal AS j ON k.id_kelas = j.id_kelas
            LEFT JOIN absen AS a ON a.id_jadwal = j.id_jadwal
            WHERE st_absen = 'I' AND a.nim = '$nim'
            UNION
            SELECT COALESCE(a.nim, '$nim') AS nim, 0 AS alpha, 0 AS ijin, COALESCE(SUM(jml_absen), 0) AS sakit
            FROM kelas AS k
            LEFT JOIN jadwal AS j ON k.id_kelas = j.id_kelas
            LEFT JOIN absen AS a ON a.id_jadwal = j.id_jadwal
            WHERE st_absen = 'S' AND a.nim = '$nim'
            ) AS a
            JOIN mahasiswa AS m ON a.nim = m.nim
            WHERE a.nim = '$nim'
            GROUP BY a.nim
        ";

        $ais = DB::select(DB::raw($sql));

        $absen = DB::table('absen')->where('nim', $nim)->get();

        $a = $ais[0]->alpha;

        if ($a > 44) {
            $ket = 'SP 3';
        } elseif ($a > 36) {
            $ket = 'SP 2';
        } elseif ($a > 18) {
            $ket = 'SP 1';
        } else {
            $ket = '-';
        }

        return view('mhs.absen.dtlAbsen')->with(['ais' => $ais, 'absen' => $absen, 'ket' => $ket]);
    }
}
