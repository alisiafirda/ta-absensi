<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class IzinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dt['izin'] = DB::table('absen AS a')
            ->join('jadwal AS j', 'a.id_jadwal', 'j.id_jadwal')
            ->join('matkul AS m', 'j.kode_matkul', 'm.kode_matkul')
            ->join('kelas AS k', 'j.id_kelas', 'k.id_kelas')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('a.*', 'j.*', 'm.*', 'k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->where('a.st_absen', 'WI')
            ->orWhere('a.st_absen', 'WS')
            ->get();

        return view('cekizin.index', $dt);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dt['i'] = DB::table('absen AS a')
            ->join('jadwal AS j', 'a.id_jadwal', 'j.id_jadwal')
            ->join('matkul AS m', 'j.kode_matkul', 'm.kode_matkul')
            ->join('kelas AS k', 'j.id_kelas', 'k.id_kelas')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->join('jwd_jm AS jw', 'jw.id_jadwal', 'j.id_jadwal')
            ->join('jam AS jm', 'jm.id_jam', 'jw.id_jam')
            ->select(DB::raw("a.*, j.*, k.*, m.*, s.semester AS smt, t.tingkat AS nm_tingkat, MIN(jm.mulai) AS mulai, MAX(jm.selesai) AS selesai"))
            // ->select('a.*', 'j.*', 'm.*', 'k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->where(['a.st_absen' => 'WI', 'a.no' => $id])
            ->orWhere(['a.st_absen' => 'WS', 'a.no' => $id])
            ->first();

        return view('cekizin.edit', $dt);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $izin = DB::table('absen')
        ->where('no', $id)
        ->first();

        if ($request->approval) {
            if ($izin->st_absen == 'WI') {
                $st = 'I';
            } elseif ($izin->st_absen == 'WS') {
                $st = 'S';
            }
        } else {
            $st = 'A';
        }

        DB::table('absen')
        ->where('no', $id)
        ->update([
            'st_absen' => $st,
        ]);

        return redirect()->route('cek-izin.index')->with('status', 'Data Berhasil Diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
