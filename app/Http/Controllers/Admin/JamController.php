<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Jam;
use DB;

class JamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dt['jam'] = DB::table('jam')
        ->get();

        return view('jam.index', $dt);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jam.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $j = new Jam;
        $cek = $this->save($j, $request);

        if ($cek) {
            return redirect()->route('jam.index')->with('status', 'Data Berhasil Disimpan!');
        } else {
            return redirect()->route('jam.index')->with('error', 'Internal Server Error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dt['j'] = Jam::findOrFail($id);

        return view('jam.edit', $dt);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $j = Jam::findOrFail($id);
        $cek = $this->save($j, $request);

        if ($cek) {
            return redirect()->route('jam.index')->with('status', 'Data Berhasil Diubah!');
        } else {
            return redirect()->route('jam.index')->with('error', 'Internal Server Error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $j = Jam::findOrFail($id);
        
        if ($j->delete()) {
            return redirect()->route('jam.index')->with('status', 'Data Berhasil Dihapus!');
        } else {
            return redirect()->route('jam.index')->with('error', 'Internal Server Error');
        }
    }

    public function save(Jam $j, $request)
    {
        $j->jam = $request->jam;
        $j->mulai = $request->jm;
        $j->selesai = $request->js;

        if ($j->save()) {
            return 1;
        } else {
            return 0;
        }
    }
}
