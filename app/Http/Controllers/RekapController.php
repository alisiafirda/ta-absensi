<?php

namespace App\Http\Controllers;

use App\Exports\AbsenExport;
use Excel;
use Illuminate\Support\Facades\DB;
use PDF;

class RekapController extends Controller
{
    public function data()
    {
        $absensi = DB::table('absen')->get();
        return view('rekap.rekap', ['absensi' => $absensi]);
    }

    public function DownloadPDF()
    {
        $absensi = DB::table('absen')->get();
        $pdf = PDF::loadview('rekap.rekapPDF', compact('absensi'));
        return $pdf->download('absensi.pdf');
    }

    public function exportIntoExcel()
    {
        return Excel::download(new AbsenExport, 'Rekap Absensi.xlsx');
    }

    public function exportIntoCSV()
    {
        return Excel::download(new AbsenExport, 'Rekap Absensi.csv');
    }

    public function calculate()
    {
        $jwd = DB::table('jadwal AS j')
            ->join('matkul AS m', 'j.kode_matkul', 'm.kode_matkul')
            ->join('jwd_jm AS jw', 'jw.id_jadwal', 'j.id_jadwal')
            ->join('jam AS jm', 'jw.id_jam', 'jm.id_jam')
            ->select(DB::raw("j.*, m.*, MIN(jm.mulai) AS mulai, MAX(jm.selesai) AS selesai"))
            ->whereRaw('DAYOFWEEK(CURDATE()) = int_hari')
            ->groupBy('j.id_jadwal')
            ->get();

        $jwd = $jwd->where('selesai', '<', date('H:i:s'));

        if (count($jwd) === 0) {
            return redirect()->back();
        }

        foreach ($jwd as $it) {
            $abs = DB::table('absen')
                ->where(['id_jadwal' => $it->id_jadwal, 'tanggal' => date('Y-m-d')])
                ->pluck('nim');

            $mhs = DB::table('mahasiswa')
                ->where('id_kelas', $it->id_kelas)
                ->whereNotIn('nim', $abs)
                ->get();

            if (count($mhs) === 0) {
                return redirect()->back();
            }

            $jam_mulai = date_create($it->mulai);
            $jam_selesai = date_create($it->selesai);

            $interval = date_diff($jam_mulai, $jam_selesai);
            $minutes = $interval->format('%H') * 60 + $interval->format('%i');
            $alpha = ceil($minutes / 50);

            foreach ($mhs as $m) {
                DB::table('absen')->insert([
                    'id_jadwal' => $it->id_jadwal,
                    'nim' => $m->nim,
                    'nama_lengkap' => $m->nama_mhs,
                    'prodi' => 'MI',
                    'tanggal' => date('Y-m-d'),
                    'matkul' => $it->nama_matkul,
                    'st_absen' => 'A',
                    'jml_absen' => $alpha,
                    'keterangan' => 'Alpha',
                ]);
            }

        }

        return redirect()->back();
    }

}
