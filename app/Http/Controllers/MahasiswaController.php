<?php

namespace App\Http\Controllers;

use App\Exports\MhsExport;
use App\Models\Mahasiswa;
use App\Models\User;
use DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MahasiswaController extends Controller
{
    public function data(Request $request)
    {
        $mhs = DB::table('mahasiswa AS m')
            ->leftJoin('kelas AS k', 'm.id_kelas', 'k.id_kelas')
            ->leftJoin('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('m.*', 'k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat');

        $where = [];
        if ($request->id_kelas != '') {
            $where['k.id_kelas'] = $request->id_kelas;
        }

        $dt['mahasiswa'] = $mhs->where($where)->get();
        
        $dt['kls'] = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->get();

        return view('mahasiswa.mahasiswa', $dt);
    }

    public function tambah()
    {
        // $dt['kls'] = DB::table('kelas')->get();
        // $dt['tingkat'] = DB::table('tingkat')->get();
        // $dt['smt'] = DB::table('semester')->get();

        $dt['kls'] = $mhs = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->get();

        return view('mahasiswa.tambah', $dt);
    }
    public function addProcess(Request $request)
    {
        //insert ke tabel user
        // $user = new \App\Models\User;
        // $user->level = 'mahasiswa';
        // $user->name = $request->namamhs;
        // $user->email = $request->email;
        // $user->password = Hash::make($request->password);
        // $user->remember_token = \Str::random(60);
        // $user->save();

        // $request->request->add(['user_id' => $user->id ]);

        $kls = DB::table('kelas')->where('id_kelas', $request->id_kelas)->first();

        $mhs = Mahasiswa::create([
            'user_id' => DB::table('mahasiswa')->orderBy('id', 'desc')->first()->id + 1,
            'nim' => $request->nim,
            'nama_mhs' => $request->namamhs,
            'prodi' => 'MI',
            'id_kelas' => $kls->id_kelas,
            'id_semester' => $kls->semester,
            'semester' => $kls->semester,
            'ttl' => $request->ttl,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat_lengkap' => $request->alamat,
            'agama' => $request->agama,
        ]);

        User::create([
            'name' => $mhs->nama_mhs,
            'email' => $request->email,
            'username' => $request->nim,
            'password' => \Hash::make($request->nim),
            'level' => 'mahasiswa',
            'mahasiswa_id' => $mhs->user_id,
        ]);

        return redirect('/mahasiswa')->with('status', 'Mahasiswa berhasil di tambah!');
    }
    public function edit($id)
    {
        $dt['kls'] = $mhs = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->get();

        $dt['mahasiswa'] = Mahasiswa::findOrFail($id);

        return view('mahasiswa.edit', $dt);
    }

    public function detail($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        return view('mahasiswa.detail', compact('mahasiswa'));
    }

    public function update(Mahasiswa $mahasiswa, Request $request)
    {
        $kls = DB::table('kelas')->where('id_kelas', $request->id_kelas)->first();

        Mahasiswa::where('id', $mahasiswa->id)->update([
            'nim' => $request->nim,
            'nama_mhs' => $request->namamhs,
            'id_kelas' => $kls->id_kelas,
            'id_semester' => $kls->semester,
            'ttl' => $request->ttl,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat_lengkap' => $request->alamat,
            'agama' => $request->agama,
            'updated_at' => date('Y-m-d'),
        ]);
        return redirect('mahasiswa')->with('status', 'Data Mahasiswa Berhasil Diubah!');
    }

    public function delete($id)
    {
        $mahasiswa = Mahasiswa::find($id);
        $mahasiswa->delete($mahasiswa);
        return redirect('mahasiswa')->with('status', 'Data Mahasiswa Berhasil Dihapus!');
    }

    public function exportAbsen($id)
    {
        $date = date('Y-m-d_H:i:s');
        return Excel::download(new MhsExport($id), "$id-$date.xlsx");
    }
}
