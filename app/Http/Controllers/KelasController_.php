<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Semester;
use App\Models\Tingkat;
use DB;
use Illuminate\Http\Request;

class KelasController extends Controller
{
    public function data()
    {
        $kelas = Kelas::all();
        $semester = Semester::all();
        $tingkat = Tingkat::all();
        return view('kelas.kelas', compact('kelas', 'semester', 'tingkat'));
    }
    public function tambah()
    {
        $semester = Semester::all();
        $tingkat = Tingkat::all();
        return view('kelas.tambah', compact('semester', 'tingkat'));
    }

    public function addProcess(Request $request)
    {
        Kelas::create([
            'nama_kelas' => $request->nmkls,
            'semester' => $request->smt,
            'tingkat' => $request->tk,
        ]);
        return redirect('/kelas')->with('status', 'Kelas berhasil di tambah!');
    }

    public function edit($id_kelas)
    {
        $semester = Semester::all();
        $tingkat = Tingkat::all();
        $kelas = Kelas::find($id_kelas);
        return view('kelas.edit', compact('kelas', 'semester', 'tingkat'));
    }

    public function update(Kelas $kelas, Request $request)
    {
        Kelas::where('id_kelas', $kelas->id_kelas)->update([
            'nama_kelas' => $request->nmkls,
            'semester' => $request->smt,
            'tingkat' => $request->tk,
            'updated_at' => date('Y-m-d'),
        ]);
        return redirect('kelas')->with('status', 'Data Kelas Berhasil Diubah!');
    }

    public function delete($id_kelas)
    {
        $kelas = Kelas::find($id_kelas);
        $kelas->delete($kelas);
        return redirect('kelas')->with('status', 'Data Kelas Berhasil Dihapus!');
    }

    public function info(Request $request, $id)
    {
        $dt['kls'] = DB::table('kelas AS k')
            ->leftJoin('semester AS s', 'k.semester', 's.id_semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->where('k.id_kelas', $id)
            ->first();

        $dt['jwd'] = DB::table('jadwal AS j')
            ->join('matkul AS m', 'm.kode_matkul', 'j.kode_matkul')
            ->where('id_kelas', $id)
            ->groupBy('m.kode_matkul')
            ->get();

        $dt['mhs'] = DB::table('kelas AS k')
            ->join('mahasiswa AS m', 'k.id_kelas', 'm.id_kelas')
            ->get();
        
        if ($request->has('matkul')) {
            $dt['absen'] = DB::table('jadwal AS j')
            ->join('absen AS a', 'j.id_jadwal', 'a.id_jadwal')
            ->where('j.kode_matkul', $request->matkul)
            ->groupBy('a.tanggal')
            ->get();
        }

        // return response()->json($dt['jwd']);
        return view('kelas.info', $dt);
    }

    public function exportRekapAbsen(Request $request)
    {
        $jwd = DB::table('matkul')->where('kode_matkul', $request->matkul)->first();

        $rekap = DB::table('mahasiswa AS m')
        ->leftJoin('absen AS a', function($j) use($request, $jwd) {
            $j->on('a.nim', 'm.nim')
            ->where(['a.tanggal' => $request->tgl, 'a.matkul' => $jwd->nama_matkul]);
        })
        ->get();

        return response()->json($rekap);
    }

}
