<?php

namespace App\Http\Controllers;

use App\Models\Matkul;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatkulController extends Controller
{
    public function data(Request $request)
    {
        $dt['matkul'] = DB::table('matkul')->get();

        return view('matkul.matkul', $dt);
    }
    public function tambah()
    {
        $tb_stat = DB::select("SHOW TABLE STATUS LIKE 'matkul'");
        $dt['nextId'] = $tb_stat[0]->Auto_increment;

        return view('matkul.tambah', $dt);
    }
    public function addProcess(Request $request)
    {
        Matkul::create([
            'nama_matkul' => $request->namamk,
            'sks' => $request->sks,
        ]);
        return redirect('/matkul')->with('status', 'Mata Kuliah berhasil di tambah!');
    }

    public function edit($kodemk)
    {
        $matkul = Matkul::find($kodemk);
        return view('matkul.edit', compact('matkul'));
    }

    public function update($kodemk, Request $request)
    {
        Matkul::where('kode_matkul', $kodemk)->update([
            'nama_matkul' => $request->namamk,
            'sks' => $request->sks,
            'updated_at' => date('Y-m-d'),
        ]);
        return redirect('matkul')->with('status', 'Data Mata Kuliah Berhasil Diubah!');
    }

    public function delete($kodemk)
    {
        $matkul = Matkul::find($kodemk);
        $matkul->delete($matkul);
        return redirect('matkul')->with('status', 'Data Mata Kuliah Berhasil Dihapus!');
    }
    public function datadua()
    {
        $matkul = DB::table('matkul')->get();

        return view('matkul2.index', ['matkul' => $matkul]);
    }

}
