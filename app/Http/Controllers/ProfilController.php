<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Mahasiswa;

class ProfilController extends Controller
{

//     public function profil()
//     {
//     $mahasiswa = Mahasiswa::all();
//      return view('profil.profilmhs',compact('mahasiswa'));
//     }
    public function profil()
    {
        $mahasiswa = DB::table('mahasiswa')->where('user_id', auth()->user()->mahasiswa_id)->first();

        return view('profil.profilmhs', compact('mahasiswa'));

    }

    public function ubahPass(Request $request)
    {
      $user = User::findOrFail(auth()->user()->id);
      $user->password = bcrypt($request->password);
      $user->save();

      return redirect()->route('prof_mhs')->with('success', 'Password berhasil diubah');
    }

    public function update_profil_mahasiswa()
    {
        $profil = Auth::user();
        $profil->update([
            'nama' => request('nama'),
            'email' => request('email'),
            'level' => request('level'),

        ]);

        Alert::success('Sukses', 'Data Berhasil Diedit');
        return redirect()->to('/halaman_admin_profil-admin');

    }

    public function update_profil_admin()
    {
        $profil = Auth::user();
        $profil->update([
            'name' => request('name'),
            'email' => request('email'),
            'level' => request('level'),

        ]);

        Alert::success('Sukses', 'Data Berhasil Diedit');
        return redirect()->to('/halaman_profil_admin');

    }

    public function update_password_admin(Request $request)
    {
        $profil = Auth::user();
        $profil->update([
            'password' => \Hash::make($request->password),

        ]);

        Alert::success('Sukses', 'Data Berhasil Diedit');
        return redirect()->to('/halaman_profil_admin');

    }

}
