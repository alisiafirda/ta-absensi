<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Semester;

class SemesterController extends Controller
{
    public function data()
    {
       $semester = Semester::all();
        
        return view('semester.semester', compact('semester'));
    }

    public function tambah()
    {
        return view('semester.tambah');
    }

    public function addProcess(Request $request)
    {
        Semester::create([
            'semester' => $request->smt
        ]);
        return redirect('/semester')->with('status', 'Semester berhasil di tambah!');
    }   

    public function edit($id_semester){
        $semester = Semester::find($id_semester);
        return view('semester.edit',compact('semester'));
    }

    public function update($id_semester, Request $request){
        Semester::where('id_semester', $id_semester)->update([
                'semester' => $request->smt,
                'updated_at' =>  date('Y-m-d')
            ]);
            return redirect('semester')->with('status', 'Data Semester Berhasil Diubah!'); 
    }

    public function delete($id_semester){
        $semester = Semester::find($id_semester);
        $semester->delete($semester);
        return redirect('semester')->with('status', 'Data Semester Berhasil Dihapus!');
  }

}
