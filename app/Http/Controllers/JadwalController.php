<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use App\Models\Jadwal;
use App\Models\Jam;
use App\Models\Matkul;
use App\Models\Semester;
use App\Models\Tingkat;
use DB;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    public function data(Request $request)
    {
        if ($request->id_kelas) {
            $jadwal = Jadwal::join('jwd_jm AS jw', 'jw.id_jadwal', 'jadwal.id_jadwal')
                ->join('jam AS jm', 'jm.id_jam', 'jw.id_jam')
                ->select(DB::raw("jadwal.*, MIN(jm.mulai) AS mulai, MAX(jm.selesai) AS selesai"))
                ->where('id_kelas', $request->id_kelas)
                ->groupBy('jadwal.id_jadwal')
                ->get();
        } else {
            $jadwal = Jadwal::join('jwd_jm AS jw', 'jw.id_jadwal', 'jadwal.id_jadwal')
                ->join('jam AS jm', 'jm.id_jam', 'jw.id_jam')
                ->select(DB::raw("jadwal.*, MIN(jm.mulai) AS mulai, MAX(jm.selesai) AS selesai"))
                ->groupBy('jadwal.id_jadwal')
                ->get();
        }

        $kls = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->get();

        return view('jadwal.jadwal', compact('jadwal', 'kls'));
    }

    public function tambah()
    {
        $dt['kls'] = $mhs = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->get();

        $dt['matkul'] = Matkul::all();
        $dt['dosen'] = Dosen::all();
        $dt['jam'] = Jam::all();

        return view('jadwal.tambah', $dt);
    }
    public function addProcess(Request $request)
    {
        $dosen = DB::table('dosen')->where('nidn', $request->nidn)->first();
        $kls = DB::table('kelas')->where('id_kelas', $request->id_kelas)->first();

        $id_jam = $request->id_jam;

        $ihari = $this->ihari($request->hari);

        if ($ihari === -1) {
            return redirect()->back()->withInput()->with('error', 'Input hari salah!');
        }

        $cekkelas = DB::table('jadwal AS j')
            ->join('jwd_jm AS jw', function ($j) use ($id_jam) {
                $j->on('jw.id_jadwal', 'j.id_jadwal')
                    ->whereIn('id_jam', $id_jam);
            })
            ->where([
                'id_kelas' => $request->id_kelas,
                'kode_matkul' => $request->matkul,
                'int_hari' => $ihari,
            ])
            ->groupBy('j.id_jadwal')
            ->count();

        if ($cekkelas > 0) {
            return redirect()->back()->withInput()->with('error', 'Jadwal tidak boleh ganda!');
        }

        $cekdosen = DB::table('jadwal AS j')
            ->join('jwd_jm AS jw', function ($j) use ($id_jam) {
                $j->on('jw.id_jadwal', 'j.id_jadwal')
                    ->whereIn('id_jam', $id_jam);
            })
            ->where([
                'nidn' => $request->nidn,
                'int_hari' => $ihari,
            ])
            ->groupBy('j.id_jadwal')
            ->count();

        if ($cekdosen > 0) {
            return redirect()->back()->withInput()->with('error', 'Dosen Ybs. sedang ada jadwal!');
        }

        $jwd = Jadwal::create([
            'id_semester' => $kls->semester,
            'id_tingkat' => $kls->tingkat,
            'id_kelas' => $kls->id_kelas,
            'nidn' => $request->nidn,
            'kode_matkul' => $request->matkul,
            'hari' => $request->hari,
            'int_hari' => $ihari,
            'nama_dosen' => $dosen->nama_dosen,
        ]);

        foreach ($request->id_jam as $v) {
            DB::table('jwd_jm')->insert([
                'id_jadwal' => $jwd->id_jadwal,
                'id_jam' => $v,
            ]);
        }

        return redirect('/jadwal')->with('status', 'Jadwal berhasil di tambah!');
    }

    public function edit($id_jadwal)
    {
        $dt['kls'] = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->get();

        $dt['matkul'] = Matkul::all();
        $dt['dosen'] = Dosen::all();
        $dt['jadwal'] = Jadwal::find($id_jadwal);
        $dt['jam'] = Jam::all();
        $dt['jwd_jm'] = DB::table('jwd_jm')->where('id_jadwal', $id_jadwal)->pluck('id_jam')->toArray();

        return view('jadwal.edit', $dt);
    }

    public function update(Jadwal $jadwal, Request $request)
    {
        $dosen = DB::table('dosen')->where('nidn', $request->nidn)->first();
        $kls = DB::table('kelas')->where('id_kelas', $request->id_kelas)->first();

        $id_jam = $request->id_jam;

        $ihari = $this->ihari($request->hari);

        if ($ihari === -1) {
            return redirect()->back()->withInput()->with('error', 'Input hari salah!');
        }

        $cekkelas = DB::table('jadwal AS j')
            ->join('jwd_jm AS jw', function ($j) use ($id_jam) {
                $j->on('jw.id_jadwal', 'j.id_jadwal')
                    ->whereIn('id_jam', $id_jam);
            })
            ->where([
                'id_kelas' => $request->id_kelas,
                'kode_matkul' => $request->matkul,
                'int_hari' => $ihari,
            ])
            ->whereNotIn('j.id_jadwal', [$jadwal->id_jadwal])
            ->groupBy('j.id_jadwal')
            ->count();

        if ($cekkelas > 0) {
            return redirect()->back()->withInput()->with('error', 'Jadwal tidak boleh ganda!');
        }

        $cekdosen = DB::table('jadwal AS j')
            ->join('jwd_jm AS jw', function ($j) use ($id_jam) {
                $j->on('jw.id_jadwal', 'j.id_jadwal')
                    ->whereIn('id_jam', $id_jam);
            })
            ->where([
                'nidn' => $request->nidn,
                'int_hari' => $ihari,
            ])
            ->whereNotIn('j.id_jadwal', [$jadwal->id_jadwal])
            ->groupBy('j.id_jadwal')
            ->count();

        if ($cekdosen > 0) {
            return redirect()->back()->withInput()->with('error', 'Dosen Ybs. sedang ada jadwal!');
        }

        Jadwal::where('id_jadwal', $jadwal->id_jadwal)->update([
            'id_semester' => $kls->semester,
            'id_tingkat' => $kls->tingkat,
            'id_kelas' => $kls->id_kelas,
            'nidn' => $request->nidn,
            'kode_matkul' => $request->matkul,
            'hari' => $request->hari,
            'int_hari' => $ihari,
            'nama_dosen' => $dosen->nama_dosen,
            'updated_at' => date('Y-m-d'),
        ]);

        DB::table('jwd_jm')->where('id_jadwal', $jadwal->id_jadwal)->delete();

        foreach ($request->id_jam as $v) {
            DB::table('jwd_jm')->insert([
                'id_jadwal' => $jadwal->id_jadwal,
                'id_jam' => $v,
            ]);
        }

        return redirect('jadwal')->with('status', 'Jadwal Berhasil Diubah!');
    }

    public function delete($id_jadwal)
    {
        $jadwal = Jadwal::find($id_jadwal);
        $jadwal->delete($jadwal);
        return redirect('jadwal')->with('status', 'Jadwal Berhasil Dihapus!');
    }

    public function datadua()
    {
        $mhs = DB::table('mahasiswa')->where('user_id', auth()->user()->mahasiswa_id)->first();
        $jadwal = Jadwal::join('jwd_jm AS jw', 'jw.id_jadwal', 'jadwal.id_jadwal')
            ->join('jam AS jm', 'jm.id_jam', 'jw.id_jam')
            ->select(DB::raw("jadwal.*, MIN(jm.mulai) AS mulai, MAX(jm.selesai) AS selesai"))
            ->groupBy('jadwal.id_jadwal')
            ->where('id_kelas', $mhs->id_kelas)
            ->get();

        return view('jadwal2.index', compact('jadwal'));

    }

    public function ihari($hri)
    {
        switch ($hri) {
            case 'Minggu':
                $i = 1;
                break;
            case 'Senin':
                $i = 2;
                break;
            case 'Selasa':
                $i = 3;
                break;
            case 'Rabu':
                $i = 4;
                break;
            case 'Kamis':
                $i = 5;
                break;
            case 'Jumat':
                $i = 6;
                break;
            case "Jum'at":
                $i = 6;
                break;
            case 'Sabtu':
                $i = 7;
                break;
            default:
                $i = -1;
                break;
        }

        return $i;
    }
}
