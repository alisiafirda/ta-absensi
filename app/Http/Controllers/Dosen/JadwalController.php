<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dt['jadwal'] = DB::table('dosen AS d')
            ->join('jadwal AS j', 'j.nidn', 'd.nidn')
            ->join('kelas AS k', 'j.id_kelas', 'k.id_kelas')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->join('matkul AS mt', 'mt.kode_matkul', 'j.kode_matkul')
            ->join('jwd_jm AS jw', 'jw.id_jadwal', 'j.id_jadwal')
            ->join('jam AS jm', 'jm.id_jam', 'jw.id_jam')
            ->select(DB::raw("j.*, d.*, k.*, mt.*, s.semester AS smt, t.tingkat AS nm_tingkat, MIN(jm.mulai) AS mulai, MAX(jm.selesai) AS selesai"))
            ->where('j.nidn', auth()->user()->nidn)
            ->whereRaw('DAYOFWEEK(CURDATE()) = int_hari')
            ->groupBy('j.id_jadwal')
            ->get();

        return view('d-jadwal.index', $dt);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dt['jadwal'] = DB::table('jadwal AS j')
            ->join('kelas AS k', 'j.id_kelas', 'k.id_kelas')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->join('matkul AS mt', 'mt.kode_matkul', 'j.kode_matkul')
            ->join('jwd_jm AS jw', 'jw.id_jadwal', 'j.id_jadwal')
            ->join('jam AS jm', 'jm.id_jam', 'jw.id_jam')
            ->select(DB::raw("j.*, k.*, mt.*, s.semester AS smt, t.tingkat AS nm_tingkat, MIN(jm.mulai) AS mulai, MAX(jm.selesai) AS selesai"))
            ->where('j.id_jadwal', $id)
            ->groupBy('j.id_jadwal')
            ->first();

        $dt['absen'] = DB::table('absen')
            ->where([
                'id_jadwal' => $id,
                'tanggal' => date('Y-m-d'),
            ])
            ->get();

        return view('d-jadwal.show', $dt);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
