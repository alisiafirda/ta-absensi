<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Hash;
use DB;

class AuthController extends Controller
{
    public function index()
    {
        return view('layout.Login');
    }

    public function proses_login(Request $request)
    {
        request()->validate(
            [
                'username' => 'required',
                'password' => 'required',
            ]);

        $kredensil = $request->only('username', 'password');

        if (Auth::attempt($kredensil)) {
            $user = Auth::user();
            if ($user->level == 'admin') {
                return redirect()->intended('/dashboard');
            } elseif ($user->level == 'mahasiswa') {
                return redirect()->intended('/absensi');
            } elseif ($user->level == 'dosen') {
                return redirect()->intended('/dosen/dashboard');
            }
            return redirect()->intended('/')->with('error', "Anda tidak punya akses!");
        }

        return redirect()->intended('/')->with('error', "Username/Password Salah!");
    }
    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('/');
    }

    public function showChangePass()
    {
        return view('admin.changepass');
    }

    public function changePass(Request $request)
    {
        $user = User::findOrFail(auth()->user()->id);

        if (!Hash::check($request->old_password, $user->password)) {
            return redirect()->back()->withInput()->with('old_password', 'Password tidak sama');
        }

        $request->validate([
            'old_password' => 'required|string',
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required',
        ]);

        $user->update([
            'password' => \Hash::make($request->password),
        ]);

        // Alert::success('Sukses','Password Berhasil Diubah');
        return redirect()->route('dos.dashboard')->with('status', 'Password Berhasil Diubah!');
    }

    public function profil()
    {
        $user = auth()->user();

        $dt = [];

        if ($user->level == 'dosen') {
            $dt['data'] = DB::table('dosen AS d')
            ->join('matkul AS m', 'd.nama_matkul', 'm.kode_matkul')
            ->where('nidn', $user->nidn)
            ->first();
        }

        return view('dosen.profil', $dt);
    }
}
