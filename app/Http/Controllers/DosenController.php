<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use App\Models\Matkul;
use App\Models\Semester;
use App\Models\Tingkat;
use App\Models\User;
use DB;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function data(Request $request)
    {
        $dosen = Dosen::join('semester', 'semester.id_semester', '=', 'dosen.semester')
            ->join('tingkat', 'tingkat.id_tingkat', '=', 'dosen.tingkat')
            ->join('kelas', 'kelas.id_kelas', '=', 'dosen.nama_kelas')
            ->select('dosen.id', 'dosen.nidn', 'dosen.nama_dosen', 'dosen.jenis_kelamin', 'dosen.hp', 'dosen.nama_matkul', 'semester.semester', 'tingkat.tingkat', 'kelas.nama_kelas');

        if ($request->id_kelas) {
            $dosen->where('kelas.id_kelas', $request->id_kelas);
        }

        $kls = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->get();

        return view('dosen.index')->with(['dosen' => $dosen->get(), 'kls' => $kls]);
    }
    public function form_tambah()
    {
        $dt['kls'] = $mhs = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->get();

        $dt['matkul'] = Matkul::all();

        return view('dosen.form_tambah', $dt);
    }
    public function addProcess(Request $request)
    {
        $kls = DB::table('kelas')->where('id_kelas', $request->id_kelas)->first();

        Dosen::create([
            'nidn' => $request->nidn,
            'nama_dosen' => $request->namadosen,
            'jenis_kelamin' => $request->jenis_kelamin,
            'hp' => $request->hp,
            'nama_matkul' => $request->matkul,
            'semester' => $kls->semester,
            'tingkat' => $kls->tingkat,
            'nama_kelas' => $kls->id_kelas,
        ]);

        User::create([
            'name' => $request->namadosen,
            'email' => $request->email,
            'username' => $request->nidn,
            'password' => \Hash::make($request->nidn),
            'level' => 'dosen',
            'nidn' => $request->nidn,
        ]);

        return redirect('/dosen')->with('status', 'Dosen berhasil di tambah!');
    }

    public function edit($nidn)
    {
        $dt['kls'] = $mhs = DB::table('kelas AS k')
            ->join('semester AS s', 's.id_semester', 'k.semester')
            ->leftJoin('tingkat AS t', 'k.tingkat', 't.id_tingkat')
            ->select('k.*', 's.semester AS smt', 't.tingkat AS nm_tingkat')
            ->get();

        $dt['matkul'] = Matkul::all();
        $dt['dosen'] = Dosen::findOrFail($nidn);
        return view('dosen.edit', $dt);
    }

    public function update(Dosen $dosen, Request $request)
    {
        $kls = DB::table('kelas')->where('id_kelas', $request->id_kelas)->first();

        Dosen::where('nidn', $dosen->nidn)->update([
            'nidn' => $request->nidn,
            'nama_dosen' => $request->namadosen,
            'jenis_kelamin' => $request->jenis_kelamin,
            'hp' => $request->hp,
            'nama_matkul' => $request->matkul,
            'semester' => $kls->semester,
            'tingkat' => $kls->tingkat,
            'nama_kelas' => $kls->id_kelas,
            'updated_at' => date('Y-m-d'),
        ]);
        return redirect('dosen')->with('status', 'Data Dosen Berhasil Diubah!');
    }

    public function delete($nidn)
    {
        $dosen = Dosen::find($nidn);
        $dosen->delete($dosen);
        return redirect('dosen')->with('status', 'Data Dosen Berhasil Dihapus!');
    }

    public function datadua()
    {
        $dosen = Dosen::join('semester', 'semester.id_semester', '=', 'dosen.semester')
            ->join('tingkat', 'tingkat.id_tingkat', '=', 'dosen.tingkat')
            ->join('kelas', 'kelas.id_kelas', '=', 'dosen.nama_kelas')
            ->select('dosen.nidn', 'dosen.nama_dosen', 'dosen.jenis_kelamin', 'dosen.hp', 'dosen.nama_matkul', 'semester.semester', 'tingkat.tingkat', 'kelas.nama_kelas')
            ->get();

        return view('dosen2.index', compact('dosen'));

    }
}
