<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $table = 'jadwal';
    protected $fillable = ['nidn', 'id_jadwal', 'id_semester', 'id_tingkat', 'id_kelas', 'kode_matkul', 'hari', 'int_hari', 'nama_dosen', 'id_jam'];
    protected $primaryKey = 'id_jadwal';

    public function semester()
    {
        return $this->hasOne(Semester::class, 'id_semester', 'id_semester');
    }

    public function kelas()
    {
        return $this->hasOne(Kelas::class, 'id_kelas', 'id_kelas');
    }

    public function matkul()
    {
        return $this->hasOne(Matkul::class, 'kode_matkul', 'kode_matkul');
    }

    public function tingkat()
    {
        return $this->hasOne(Tingkat::class, 'id_tingkat', 'id_tingkat');
    }

    public function dosen()
    {
        return $this->hasOne(Dosen::class, 'nama_dosen', 'nama_dosen');
    }
}
