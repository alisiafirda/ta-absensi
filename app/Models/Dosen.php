<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = 'dosen';
    protected $fillable = ['nidn','nama_dosen','jenis_kelamin','hp','nama_matkul','semester','tingkat','nama_kelas'];
    protected $primaryKey = 'id';

    public function semester()
        {
        return $this->hasOne(Semester::class, 'id_semester', 'semester');
    }

    public function kelas()
    {
    return $this->hasOne(Kelas::class, 'id_kelas', 'nama_kelas');
    }

    public function matkul()
    {
    return $this->hasOne(Matkul::class, 'kode_matkul', 'nama_matkul');
    }

    public function tingkat()
    {
    return $this->hasOne(Tingkat::class, 'id_tingkat', 'tingkat');
    }
}
