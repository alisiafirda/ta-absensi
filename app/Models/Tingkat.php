<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tingkat extends Model
{
    protected $table = 'tingkat';
    protected $fillable = ['id_tingkat','tingkat'];
    protected $primaryKey = 'id_tingkat';

    public function jadwal()
        {
        return $this->hasMany(Jadwal::class);
    }
}
