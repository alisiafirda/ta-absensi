<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jam extends Model
{
    use HasFactory;

    protected $table = 'jam';
    protected $primaryKey = 'id_jam';
    protected $fillable = ['jam', 'mulai', 'selesai'];
    public $timestamps = false;
}
