<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';
    protected $fillable = ['id_kelas','nama_kelas','semester', 'tingkat'];
    protected $primaryKey = 'id_kelas';

    public function semester()
        {
        return $this->hasOne(Semester::class, 'id_semester', 'semester');
    }

    public function tingkat()
    {
    return $this->hasOne(Tingkat::class, 'id_tingkat', 'tingkat');
    }
   
}
