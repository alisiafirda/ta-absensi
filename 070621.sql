/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.16-MariaDB : Database - absensi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`absensi` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `absensi`;

/*Table structure for table `absen` */

DROP TABLE IF EXISTS `absen`;

CREATE TABLE `absen` (
  `no` int(20) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(11) NOT NULL,
  `nim` varchar(10) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `prodi` varchar(30) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `matkul` varchar(30) DEFAULT NULL,
  `st_absen` char(4) DEFAULT NULL,
  `jml_absen` int(11) DEFAULT NULL,
  `bukti` text DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `absen` */

insert  into `absen`(`no`,`id_jadwal`,`nim`,`nama_lengkap`,`prodi`,`tanggal`,`matkul`,`st_absen`,`jml_absen`,`bukti`,`keterangan`) values 
(1,0,'1931733076','Alisia Firda S','Manajemen Informatika','2021-02-05','B.Indonesia','H',NULL,NULL,'Hadir'),
(6,1,'1931733055','Mark','MI','2021-06-16','B.indonesia','H',NULL,NULL,'Hadir'),
(7,1,'1931733055','Mark','MI','2021-06-16','B.indonesia','H',NULL,NULL,'Hadir'),
(8,2,'1931733055','Mark','MI','2021-06-17','B.Inggris','H',NULL,NULL,'Hadir'),
(9,3,'1931733057','Yeni','MI','2021-06-17','Kewarganegaraan','H',NULL,NULL,'Hadir'),
(10,5,'1931733055','Mark','MI','2021-06-20','Desain Grafis','I',NULL,'1931733055-5-1624205232504970284.jpg','Acara Keluarga'),
(11,6,'1931733055','Mark','MI','2021-06-21','K3','A',1,NULL,'Alpha Hadir'),
(12,6,'1931733055','Mark','MI','2021-06-21','K3','H',NULL,NULL,'Hadir'),
(13,7,'1931733055','Mark','MI','2021-06-22','Desain Grafis','A',1,NULL,'Alpha Hadir'),
(14,7,'1931733055','Mark','MI','2021-06-22','Desain Grafis','H',NULL,NULL,'Hadir'),
(15,8,'1931733055','Mark','MI','2021-06-22','K3','A',1,NULL,'Alpha Hadir'),
(16,8,'1931733055','Mark','MI','2021-06-22','K3','H',NULL,NULL,'Hadir'),
(17,1,'1931733055','Mark','MI','2021-06-23','B.Indonesia','A',1,NULL,'Alpha Hadir'),
(18,1,'1931733055','Mark','MI','2021-06-23','B.Indonesia','H',NULL,NULL,'Hadir'),
(20,17,'1931733055','Mark','MI','2021-06-25','B.Inggris','I',4,'1931733055-17-16246384781335453611.jpg','Acara');

/*Table structure for table `dosen` */

DROP TABLE IF EXISTS `dosen`;

CREATE TABLE `dosen` (
  `nidn` decimal(12,0) NOT NULL,
  `nama_dosen` varchar(50) DEFAULT NULL,
  `jenis_kelamin` varchar(12) DEFAULT NULL,
  `hp` decimal(12,0) DEFAULT NULL,
  `nama_matkul` varchar(50) DEFAULT NULL,
  `semester` varchar(20) DEFAULT NULL,
  `tingkat` varchar(20) DEFAULT NULL,
  `nama_kelas` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`nidn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dosen` */

insert  into `dosen`(`nidn`,`nama_dosen`,`jenis_kelamin`,`hp`,`nama_matkul`,`semester`,`tingkat`,`nama_kelas`,`updated_at`,`created_at`) values 
(871294,'Okesiap','Perempuan',878812321,'3','1','2','3','2021-06-24 00:00:00','2021-06-24 13:05:36'),
(1990012,'Sugar','Laki-Laki',899,'1','1','1','1','2021-07-04 18:38:48','2021-07-04 18:38:48'),
(33333333,'Helmi','Laki-Laki',456654,'5','3','1','12','2021-06-23 21:09:41','2021-06-23 21:09:41'),
(1111111111,'Brian','Laki-Laki',7654321,'4','4','2','12','2021-06-23 21:01:09','2021-06-23 21:01:09'),
(1931733077,'Bayu','Laki-Laki',8778998,'Desain Grafis','Genap 2020/2021','I','B','2021-06-23 00:00:00','2021-06-17 21:18:55'),
(1931733088,'Selly','Perempuan',2147483647,'2','4','3','4','2021-06-23 00:00:00','2021-06-17 21:19:24'),
(1998899755,'Adrian','Laki-Laki',7777654,'2','3','3','4','2021-06-23 00:00:00','2021-06-23 20:04:23'),
(1998899799,'Michele','Perempuan',2345654,NULL,'5','3','3','2021-06-23 00:00:00','2021-06-23 19:30:00');

/*Table structure for table `hari` */

DROP TABLE IF EXISTS `hari`;

CREATE TABLE `hari` (
  `id_hari` int(20) NOT NULL AUTO_INCREMENT,
  `hari` varchar(20) DEFAULT NULL,
  `int_hari` int(20) DEFAULT NULL,
  PRIMARY KEY (`id_hari`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `hari` */

insert  into `hari`(`id_hari`,`hari`,`int_hari`) values 
(1,'Minggu',1),
(2,'Senin',2),
(3,'Selasa',3),
(4,'Rabu',4),
(5,'Kamis',5),
(6,'Jumat',6),
(7,'Sabtu',7);

/*Table structure for table `jadwal` */

DROP TABLE IF EXISTS `jadwal`;

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `id_semester` int(11) NOT NULL,
  `id_tingkat` int(20) DEFAULT NULL,
  `id_kelas` int(11) NOT NULL,
  `nidn` int(15) DEFAULT NULL,
  `kode_matkul` int(20) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `nama_dosen` varchar(50) DEFAULT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `int_hari` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `jadwal` */

insert  into `jadwal`(`id_jadwal`,`id_semester`,`id_tingkat`,`id_kelas`,`nidn`,`kode_matkul`,`hari`,`nama_dosen`,`jam_mulai`,`jam_selesai`,`updated_at`,`created_at`,`int_hari`) values 
(1,1,1,1,1931733077,1,'Rabu','Bayu','12:00:00','15:10:00','2021-06-23 00:00:00',NULL,4),
(2,1,1,2,1931733088,1,'Kamis','Selly','12:00:00','15:00:00','2021-06-23 00:00:00',NULL,5),
(6,1,3,1,1931733077,2,'Senin','Bayu','22:00:00','23:00:00','2021-06-23 00:00:00','2021-06-21 22:05:12',2),
(15,1,2,2,1998899799,2,'Jumat','1998899799','08:00:00','13:00:00','2021-06-24 00:00:00','2021-06-23 20:20:09',6),
(16,3,2,3,NULL,2,'Selasa','Adrian','12:00:00','15:00:00','2021-06-23 00:00:00','2021-06-23 20:41:55',3),
(17,1,1,1,1931733077,2,'Jumat','Bayu','21:00:00','24:00:00',NULL,NULL,6);

/*Table structure for table `kelas` */

DROP TABLE IF EXISTS `kelas`;

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelas` varchar(15) NOT NULL,
  `semester` varchar(30) DEFAULT NULL,
  `tingkat` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `kelas` */

insert  into `kelas`(`id_kelas`,`nama_kelas`,`semester`,`tingkat`,`updated_at`,`created_at`) values 
(1,'A','1','1',NULL,NULL),
(2,'B','1',NULL,NULL,NULL),
(3,'C','1',NULL,NULL,NULL),
(4,'D','1',NULL,NULL,NULL),
(5,'E','1',NULL,NULL,NULL),
(6,'A','2',NULL,NULL,NULL),
(7,'B','2',NULL,NULL,NULL),
(8,'C','2',NULL,NULL,NULL),
(12,'A','3',NULL,'2021-06-22 12:26:49','2021-06-22 12:26:49'),
(13,'B','3',NULL,'2021-06-22 12:27:02','2021-06-22 12:27:02'),
(16,'J','4','2','2021-06-24 17:26:57','2021-06-24 17:26:57'),
(18,'M','1','2','2021-06-24 17:31:28','2021-06-24 17:31:28'),
(19,'N','1','1','2021-06-24 17:35:10','2021-06-24 17:35:10'),
(20,'hh','Genap 2020/2021','III','2021-06-24 00:00:00','2021-06-24 17:36:29'),
(22,'P','Ganjil 2020/2021','I','2021-06-24 00:00:00','2021-06-24 18:23:32'),
(23,'Y','Genap 2020/2021','III','2021-06-24 00:00:00','2021-06-24 18:28:25');

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_semester` int(11) DEFAULT NULL,
  `nim` varchar(10) DEFAULT NULL,
  `nama_mhs` varchar(50) DEFAULT NULL,
  `prodi` varchar(30) DEFAULT NULL,
  `semester` varchar(2) DEFAULT NULL,
  `ttl` varchar(30) DEFAULT NULL,
  `jenis_kelamin` varchar(12) DEFAULT NULL,
  `alamat_lengkap` longtext DEFAULT NULL,
  `agama` varchar(12) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`id`,`user_id`,`id_kelas`,`id_semester`,`nim`,`nama_mhs`,`prodi`,`semester`,`ttl`,`jenis_kelamin`,`alamat_lengkap`,`agama`,`updated_at`,`created_at`) values 
(2,2,0,0,'1931733087','Ratna Dwi W','Manajemen Informatika','7','Kediri, 03 Januari 2000','Perempuan','Bujel, Kota Kediri','Islam','2021-04-05 00:00:00',NULL),
(3,3,0,0,'1931733082','Virginia Abrinsa','Manajemen Informatika','5','Kediri, 23 September 1999','Perempuan','Gurah, Kab. Kediri','Islam',NULL,NULL),
(10,5,2,2,'1931733057','Yeni','MI','6','Kediri, 12','P','kediri','islam','2021-05-30 04:41:53','2021-05-30 04:41:53'),
(11,6,1,1,'1931733055','Mark','MI','1','Kediri','Laki-laki','Gurah','Islam','2021-06-29 00:00:00','2021-06-15 21:52:41'),
(12,NULL,1,1,'1931733056','Andra Maulana','MI','1','Kediri, 3 Desember 2000','Laki-laki','Gurah','Islam','2021-06-29 20:25:08','2021-06-29 20:25:08'),
(15,13,1,1,'1931733056','Ulil','MI','1','Kediri, 4 Desember 2000','Laki-Laki','Gurah','Islam','2021-06-30 22:48:30','2021-06-30 22:48:30'),
(16,16,1,1,'1931733056','Ulil','MI','1','Kediri, 4 Desember 2000',NULL,'Gurah','Islam','2021-06-30 00:00:00','2021-06-30 22:50:08'),
(17,17,1,1,'1931733090','Anjas','MI','1','Kediri, 4 Desember 2000','Laki-Laki','Klaten','Islam','2021-07-04 06:33:48','2021-07-04 06:33:48');

/*Table structure for table `matkul` */

DROP TABLE IF EXISTS `matkul`;

CREATE TABLE `matkul` (
  `kode_matkul` int(20) NOT NULL AUTO_INCREMENT,
  `nama_matkul` varchar(50) DEFAULT NULL,
  `sks` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kode_matkul`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `matkul` */

insert  into `matkul`(`kode_matkul`,`nama_matkul`,`sks`,`updated_at`,`created_at`) values 
(1,'B.Indonesia','4',NULL,NULL),
(2,'B.Inggris','4',NULL,NULL),
(3,'Kewarganegaraan','8',NULL,NULL),
(4,'K3','6','2021-05-05 01:11:43','2021-05-05 01:11:43'),
(5,'Desain Grafis','4','2021-06-17 21:22:44','2021-06-17 21:22:44'),
(7,'binggris','5','2021-06-23 00:00:00','2021-06-23 14:38:32');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

/*Table structure for table `pengumuman` */

DROP TABLE IF EXISTS `pengumuman`;

CREATE TABLE `pengumuman` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT NULL,
  `pengumuman` longtext DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pengumuman` */

insert  into `pengumuman`(`id`,`judul`,`pengumuman`,`updated_at`,`created_at`) values 
(1,'pengumuman','matkul desain libur','2021-06-17 00:00:00','2021-06-17 20:51:43'),
(2,'pengumuman','matkul bin diundur','2021-06-17 20:52:31','2021-06-17 20:52:31');

/*Table structure for table `semester` */

DROP TABLE IF EXISTS `semester`;

CREATE TABLE `semester` (
  `id_semester` int(11) NOT NULL AUTO_INCREMENT,
  `semester` varchar(30) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_semester`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `semester` */

insert  into `semester`(`id_semester`,`semester`,`updated_at`,`created_at`) values 
(1,'Ganjil 2020/2021','2021-06-22 00:00:00',NULL),
(3,'Ganjil 2019/2020','2021-06-22 00:00:00','2021-06-17 20:45:50'),
(4,'Genap 2020/2021','2021-06-17 20:46:12','2021-06-17 20:46:12'),
(5,'Genap 2019/2020','2021-06-22 12:25:35','2021-06-22 12:25:35');

/*Table structure for table `tingkat` */

DROP TABLE IF EXISTS `tingkat`;

CREATE TABLE `tingkat` (
  `id_tingkat` int(20) NOT NULL AUTO_INCREMENT,
  `tingkat` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_tingkat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tingkat` */

insert  into `tingkat`(`id_tingkat`,`tingkat`) values 
(1,'I'),
(2,'II'),
(3,'III');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mahasiswa_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`level`,`name`,`username`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`,`mahasiswa_id`) values 
(3,'mahasiswa','a','alisia','a@gmail.com',NULL,'$2y$10$QJT/b4WE1By83WMK6iEm8ONHRfdNw10fk4AolWcEWPaReIGzsM46u','B4dElkFQ3vzyfUyD2CXY8p3M1cW6piHd1KpLq9UtO4OP9FZkPX3wtU1ccQNO','2021-05-23 07:43:38','2021-05-23 07:43:38',NULL),
(5,'mahasiswa','w','','w@gmail.com',NULL,'$2y$10$EU5YOJUkjlA.jmsQezzIleGLP9vWMrCB6xk.2xKAEAK/I1phdzbrO','YJQnaiJQMQtvY7nKnCwu4aEka1e8ZphJY2cua13VA2FDmw4XnWyxADP1wjh6','2021-05-23 07:57:53','2021-05-23 07:57:53',NULL),
(6,'mahasiswa','Yeni','1931733057',NULL,NULL,'$2y$10$elzKAdFRSCUm89tBH2b9SeibOftHkMJwYYqGW9x00QxEY5fEstK7u',NULL,'2021-05-30 04:43:28','2021-05-30 04:43:28',5),
(7,'mahasiswa',NULL,'12',NULL,NULL,'$2y$10$B8KjvGymjJl8kWiTmvwKle0astcwSs1BBjvLJi5uSYdiQrHIs3zn6',NULL,'2021-05-30 05:24:55','2021-05-30 05:24:55',NULL),
(8,'mahasiswa',NULL,'13',NULL,NULL,'$2y$10$fyq9uobE4jr7fhwZlL4jhO1rXDAE87VrptGVdArb5sGdN8hn6giAi',NULL,'2021-05-30 05:25:46','2021-05-30 05:25:46',NULL),
(10,'admin','firda safira','safira','s@gmail',NULL,'$2y$10$Uyqj1Cvl87KYvWB3cjJGD.uyZkAtBCa1pyuaQyqFa.Aqy2EM6rgJu',NULL,'2021-06-05 06:39:10','2021-06-07 00:00:00',NULL),
(13,'admin','firda','firday','f@gmail',NULL,'$2y$10$YqAOvVqKVD14YITjAzHMN.UWj5bfmzbaLYHGp4cTP8qTjYDQR6WyW',NULL,NULL,'2021-06-07 00:00:00',NULL),
(14,'admin','ITSTAFF','itstaff','itstaff@mail.com',NULL,'$2y$10$pVmPyK7e.9g.YbKFGbZhve9zAeiu45X4c0ebtsX8hYVDemtQOIcQO',NULL,'2021-06-09 19:57:28','2021-07-06 21:14:16',NULL),
(15,'mahasiswa','MARK','mark','mark@mail.com',NULL,'$2y$10$u.cM/6OxAwisZM5FtvqKJ.Bpa50n5mAZUP1n1ydVF5a2FNo76B9QW',NULL,'2021-06-09 19:57:32','2021-06-29 22:40:52',6),
(17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(18,'admin','admin prodi','admin','admin@gmail.com',NULL,'$2y$10$kSegur/uaesamfuV5HxNtObHMbVHs.y8ZsP8fXwEugnTbBdxK67S6',NULL,'2021-06-17 20:54:13','2021-06-23 13:33:04',NULL),
(19,'mahasiswa',NULL,'1931733056',NULL,NULL,'$2y$10$.OAerhN4mBy7zO223yk8J.tMa.UjkbtqMWWzO7rBYXGURZBYDNi3W',NULL,'2021-06-29 20:25:08','2021-06-29 20:25:08',NULL),
(22,'mahasiswa','Ulil','1931733056','ulil@mail.com',NULL,'$2y$10$GbzCFRWDqPV4sYo/8u6md.BrTt/nx1PJE8Ac.uWtX5DTM2v.XhP2K',NULL,'2021-06-30 22:48:30','2021-06-30 22:48:30',13),
(23,'mahasiswa','Ulil','1931733056','ulil@mail.com',NULL,'$2y$10$fH4/58RTeZuaaWLt9f.lC.cS0Hn9w77TaaZQhqJTkJLvAtnKZCH0m',NULL,'2021-06-30 22:50:08','2021-06-30 22:50:08',16),
(24,'mahasiswa','Anjas','1931733090','anjas@mail.com',NULL,'$2y$10$yALgYmjuDNo43gHVQZAlCeTGfujJ8vQNBQ8PsRAvlPXqCBDJ3fDQe',NULL,'2021-07-04 06:33:48','2021-07-04 06:33:48',17),
(25,'admin','itstaff2','itstaff2','itstaff2@mail.com',NULL,'$2y$10$i5N333c/UIxxu4309wTI3eTJ5ReEXvirR/UCmCShjAnzX/IndsSJ.',NULL,'2021-07-04 18:39:26','2021-07-04 18:39:26',NULL),
(26,'admin','itstaff3','itstaff3','itstaff3@mail.com',NULL,'$2y$10$CCf.NBbElhbi9/ITdAVK4ObSJBGg3fAwNvPkMSafEySs9N1iYreve',NULL,'2021-07-04 18:41:16','2021-07-04 00:00:00',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
