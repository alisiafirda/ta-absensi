<?php

use App\Http\Controllers\AbsenController;
use App\Http\Controllers\Admin\IzinController as AdmIzinController;
use App\Http\Controllers\Admin\JamController as AdmJamController;
use App\Http\Controllers\DosenController;
use App\Http\Controllers\Dosen\JadwalController as DosJadwalController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\MatkulController;
use App\Http\Controllers\Mhs\AbsenController as MhsAbsenController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\RekapController;
use App\Http\Controllers\SemesterController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;

// mhs
use App\Http\Controllers\ViewabsenController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('layout.Landing');
})->name('login');

//LOGIN
//Route::get('/login','App\Http\Controllers\AuthController@index')->name('login');
Route::post('/proses_login', 'App\Http\Controllers\AuthController@proses_login')->name('proses_login');
Route::get('/logout', 'App\Http\Controllers\AuthController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['cek_login:admin']], function () {

//BAGIAN ADMIN
        Route::get('/dashboard', function () {
            return view('dashboard');
        });

//adminprodi
        Route::get('/halaman_admin', [UserController::class, 'data']);
        Route::get('/admin/tambah', [UserController::class, 'tambah']);
        Route::post('/add_admin', [UserController::class, 'addProcess']);
        Route::get('/edit-admin/{admin:id}', [UserController::class, 'edit']);
        Route::post('/update-admin/{admin:id}', [UserController::class, 'update']);
        Route::get('/hapus-admin/{admin:id}', [UserController::class, 'delete']);
        Route::get('/edit-password/{admin:id}', [UserController::class, 'editpsw']);
        Route::post('/update-password-admin/{admin:id}', [UserController::class, 'update_password_admin']);

//dosen
        Route::get('/dosen', [DosenController::class, 'data']);
        Route::get('/dosen/form-tambah', [DosenController::class, 'form_tambah']);
        Route::post('/add_dosen', [DosenController::class, 'addProcess']);
        Route::get('/edit-dosen/{dosen:nidn}', [DosenController::class, 'edit']);
        Route::post('/update-dosen/{dosen:nidn}', [DosenController::class, 'update']);
        Route::get('/hapus-dosen/{dosen:nidn}', [DosenController::class, 'delete']);

//jadwal
        Route::get('/jadwal', [JadwalController::class, 'data']);
        Route::get('/jadwal/tambah', [JadwalController::class, 'tambah']);
        Route::post('/add_jadwal', [JadwalController::class, 'addProcess']);
        Route::get('/edit-jadwal/{jadwal:id_jadwal}', [JadwalController::class, 'edit']);
        Route::post('/update-jadwal/{jadwal:id_jadwal}', [JadwalController::class, 'update']);
        Route::get('/hapus-jadwal/{jadwal:id_jadwal}', [JadwalController::class, 'delete']);

//kelas
        Route::get('/kelas', [KelasController::class, 'data']);
        Route::get('/kelas/tambah', [KelasController::class, 'tambah']);
        Route::post('/add_kelas', [KelasController::class, 'addProcess']);
        Route::get('/edit-kelas/{kelas:id_kelas}', [KelasController::class, 'edit']);
        Route::post('/update-kelas/{kelas:id_kelas}', [KelasController::class, 'update']);
        Route::get('/hapus-kelas/{kelas:id_kelas}', [KelasController::class, 'delete']);
        Route::get('/kelas/info/{id}', [KelasController::class, 'info'])->name('admin.kelas.info');
        Route::get('/kelas/rekap-absen/{id}', [KelasController::class, 'exportRekapAbsen'])->name('admin.kelas.rekapAbsen');

//semester
        Route::get('/semester', [SemesterController::class, 'data']);
        Route::get('/semester/tambah', [SemesterController::class, 'tambah']);
        Route::post('/add_semester', [SemesterController::class, 'addProcess']);
        Route::get('/edit-semester/{semester:id_semester}', [SemesterController::class, 'edit']);
        Route::post('/update-semester/{semester:id_semester}', [SemesterController::class, 'update']);
        Route::get('/hapus-semester/{semester:id_semester}', [SemesterController::class, 'delete']);

//mahasiswa
        Route::get('/mahasiswa', [MahasiswaController::class, 'data']);
        Route::get('/mahasiswa/tambah', [MahasiswaController::class, 'tambah']);
        Route::post('/add_mahasiswa', [MahasiswaController::class, 'addProcess']);
        Route::get('/edit-mhs/{mahasiswa:id}', [MahasiswaController::class, 'edit']);
        Route::post('/update-mhs/{mahasiswa:id}', [MahasiswaController::class, 'update']);
        Route::get('/hapus-mhs/{mahasiswa:id}', [MahasiswaController::class, 'delete']);
        Route::get('/detail-mahasiswa/{mahasiswa:id}', [MahasiswaController::class, 'detail']);
        Route::get('/export-absen-mhs/{nim}', [MahasiswaController::class, 'exportAbsen'])->name('mhs.exportAbsen');

//matkul
        Route::get('/matkul', [MatkulController::class, 'data']);
        Route::get('/matkul/tambah', [MatkulController::class, 'tambah']);
        Route::post('/add_matkul', [MatkulController::class, 'addProcess']);
        Route::get('/edit-matkul/{matkul:kode_matkul}', [MatkulController::class, 'edit']);
        Route::post('/update-matkul/{matkul:kode_matkul}', [MatkulController::class, 'update']);
        Route::get('/hapus-matkul/{matkul:kode_matkul}', [MatkulController::class, 'delete']);

// //pengumuman
        //         Route::get('/pengumuman', [PengumumanController::class, 'data']);
        //         Route::get('/pengumuman/tambah', [PengumumanController::class, 'tambah']);
        //         Route::post('/add_peng', [PengumumanController::class, 'addProcess']);
        //         Route::get('/edit-peng/{pengumuman:id}', [PengumumanController::class, 'edit']);
        //         Route::post('/update-peng/{pengumuman:id}', [PengumumanController::class, 'update']);
        //         Route::get('/hapus-peng/{pengumuman:id}', [PengumumanController::class, 'delete']);

        // izin
        Route::resource('/cek-izin', AdmIzinController::class);
        Route::resource('/jam', AdmJamController::class);

//rekap

//viewabsen
        Route::get('/index', [ViewabsenController::class, 'data']);

        Route::get('/calculate', [RekapController::class, 'calculate'])->name('calculate');

    });
});

//BAGIAN MAHASISWA
Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['cek_login:mahasiswa']], function () {

//home mahasiswa
        Route::get('/awal', function () {
            return view('dashboard2');
        })->name('home');

//show data
        Route::get('/dosen2', [DosenController::class, 'datadua'])->name('dosen2');
        Route::get('/jadwal2', [JadwalController::class, 'datadua'])->name('jadwal2');
        // Route::get('/matkul2', [MatkulController::class, 'datadua'])->name('matkul2');
        // Route::get('/pengumuman2', [PengumumanController::class, 'datadua'])->name('pengumuman2');

        Route::get('/absensi', [MhsAbsenController::class, 'index'])->name('absensi');
        Route::post('/hadir', [MhsAbsenController::class, 'hadir'])->name('hadir');
        Route::get('/izin', [MhsAbsenController::class, 'formIzin'])->name('formIzin');
        Route::post('/izin', [MhsAbsenController::class, 'izin'])->name('izin');

        Route::get('/absen', [AbsenController::class, 'index']);

        Route::get('/detail-absensi', [MhsAbsenController::class, 'dtlAbsen'])->name('mhs.dtlAbsen');

//profil
        Route::get('/profil_mahasiswa', [ProfilController::class, 'profil'])->name('prof_mhs');
        Route::post('/ubah-password', [ProfilController::class, 'ubahPass'])->name('mhs.profil.ubahPass');
// Route::get('/admin/tambah', [UserController::class, 'tambah']);
        // Route::post('/add_admin', [UserController::class, 'addProcess']);
        // Route::get('/edit-admin/{admin:id}', [UserController::class, 'edit']);
        // Route::post('/update-admin/{admin:id}', [UserController::class, 'update']);
        // Route::get('/hapus-admin/{admin:id}', [UserController::class, 'delete']);

    });

    Route::get('/rekap', [RekapController::class, 'data']);

//export
    Route::get('/download-pdf', [RekapController::class, 'DownloadPDF']);
    Route::get('/export-excel', [RekapController::class, 'exportIntoExcel']);
    Route::get('/export-csv', [RekapController::class, 'exportIntoCSV']);
});

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['cek_login:dosen'], 'prefix' => 'dosen'], function () {
        Route::get('dashboard', function () {
            return view('dashboard');
        })->name('dos.dashboard');
        Route::resource('dosen-jadwal', DosJadwalController::class);
    });
});

Route::group(['middleware' => ['auth']], function () {
        Route::get('ubah-password', [AuthController::class, 'showChangePass'])->name('admin.changepass');
        Route::patch('ubah-password', [AuthController::class, 'changePass'])->name('admin.ubahpwd');

        Route::get('profil', [AuthController::class, 'profil'])->name('admin.profil');
});
