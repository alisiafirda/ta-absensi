/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.16-MariaDB : Database - absensi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`absensi` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `absensi`;

/*Table structure for table `absen` */

DROP TABLE IF EXISTS `absen`;

CREATE TABLE `absen` (
  `no` int(20) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(11) NOT NULL,
  `nim` varchar(10) DEFAULT NULL,
  `nama_lengkap` varchar(50) DEFAULT NULL,
  `prodi` varchar(30) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `matkul` varchar(30) DEFAULT NULL,
  `st_absen` char(4) DEFAULT NULL,
  `jml_absen` int(11) DEFAULT NULL,
  `bukti` text DEFAULT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

/*Data for the table `absen` */

insert  into `absen`(`no`,`id_jadwal`,`nim`,`nama_lengkap`,`prodi`,`tanggal`,`matkul`,`st_absen`,`jml_absen`,`bukti`,`keterangan`) values 
(1,0,'1931733076','Alisia Firda S','Manajemen Informatika','2021-02-05','B.Indonesia','I',NULL,NULL,'Hadir'),
(6,1,'1931733045','Mark','MI','2021-06-16','B.indonesia','I',NULL,NULL,'Hadir'),
(7,1,'1931733045','Mark','MI','2021-06-16','B.indonesia','I',NULL,NULL,'Hadir'),
(8,2,'1931733055','Mark','MI','2021-06-17','B.Inggris','I',NULL,NULL,'Hadir'),
(9,3,'1931733057','Yeni','MI','2021-06-17','Kewarganegaraan','I',NULL,NULL,'Hadir'),
(10,5,'1931733055','Mark','MI','2021-06-20','Desain Grafis','I',NULL,'1931733055-5-1624205232504970284.jpg','Acara Keluarga'),
(11,6,'1931733055','Mark','MI','2021-06-21','K3','I',1,NULL,'Alpha Hadir'),
(12,6,'1931733055','Mark','MI','2021-06-21','K3','I',NULL,NULL,'Hadir'),
(13,7,'1931733055','Mark','MI','2021-06-22','Desain Grafis','I',1,NULL,'Alpha Hadir'),
(14,7,'1931733055','Mark','MI','2021-06-22','Desain Grafis','I',NULL,NULL,'Hadir'),
(15,8,'1931733055','Mark','MI','2021-06-22','K3','I',1,NULL,'Alpha Hadir'),
(16,8,'1931733055','Mark','MI','2021-06-22','K3','I',NULL,NULL,'Hadir'),
(17,1,'1931733055','Mark','MI','2021-06-23','B.Indonesia','I',1,NULL,'Alpha Hadir'),
(18,1,'1931733055','Mark','MI','2021-06-23','B.Indonesia','I',NULL,NULL,'Hadir'),
(20,17,'1931733055','Mark','MI','2021-06-25','B.Inggris','I',4,'1931733055-17-16246384781335453611.jpg','Acara'),
(21,11,'1931733068','Niko Irham','MI','2021-05-17','Basis Data Lanjut','I',1,NULL,'Alpha Hadir'),
(22,11,'1931733068','Niko Irham','MI','2021-05-17','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(23,11,'1931733069','Renata Kusuma','MI','2021-05-17','Basis Data Lanjut','I',1,NULL,'Alpha Hadir'),
(24,11,'1931733069','Renata Kusuma','MI','2021-05-17','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(25,14,'1931733076','Alisia Firda S','MI','2021-05-18','Pemrograman Mobile','I',1,NULL,'Alpha Hadir'),
(26,14,'1931733076','Alisia Firda S','MI','2021-05-18','Pemrograman Mobile','I',NULL,NULL,'Hadir'),
(27,14,'1931733107','Dendy Fajar P','MI','2021-05-18','Pemrograman Mobile','I',1,NULL,'Alpha Hadir'),
(28,14,'1931733107','Dendy Fajar P','MI','2021-05-18','Pemrograman Mobile','I',NULL,NULL,'Hadir'),
(29,11,'1931733066','Efendi Mulya','MI','2021-06-21','Basis Data Lanjut','I',3,NULL,'Alpha Hadir'),
(30,11,'1931733066','Efendi Mulya','MI','2021-06-21','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(31,11,'1931733067','Salma Aulia','MI','2021-06-21','Basis Data Lanjut','I',3,NULL,'Alpha Hadir'),
(32,11,'1931733067','Salma Aulia','MI','2021-06-21','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(33,11,'1931733069','Renata Kusuma','MI','2021-06-21','Basis Data Lanjut','I',3,NULL,'Alpha Hadir'),
(34,11,'1931733069','Renata Kusuma','MI','2021-06-21','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(35,11,'1931733068','Niko Irham','MI','2021-06-21','Basis Data Lanjut','I',3,NULL,'Alpha Hadir'),
(36,11,'1931733068','Niko Irham','MI','2021-06-21','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(37,11,'1931733070','Abimanyu','MI','2021-06-21','Basis Data Lanjut','I',3,NULL,'Alpha Hadir'),
(38,11,'1931733070','Abimanyu','MI','2021-06-21','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(39,11,'1931733071','Benita Della','MI','2021-06-21','Basis Data Lanjut','I',3,NULL,'Alpha Hadir'),
(40,11,'1931733071','Benita Della','MI','2021-06-21','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(41,11,'1931733072','Faria Qoriatul','MI','2021-06-21','Basis Data Lanjut','I',3,NULL,'Alpha Hadir'),
(42,11,'1931733072','Faria Qoriatul','MI','2021-06-21','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(43,11,'1931733073','Ananta Darma','MI','2021-06-21','Basis Data Lanjut','I',3,NULL,'Alpha Hadir'),
(44,11,'1931733073','Ananta Darma','MI','2021-06-21','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(45,11,'1931733074','Jefri Nichol','MI','2021-06-21','Basis Data Lanjut','I',3,NULL,'Alpha Hadir'),
(46,11,'1931733074','Jefri Nichol','MI','2021-06-21','Basis Data Lanjut','I',NULL,NULL,'Hadir'),
(47,4,'1931733053','Devano Putra','MI','2021-07-08','Kewarganegaraan','I',3,NULL,'Alpha Hadir'),
(48,4,'1931733053','Devano Putra','MI','2021-07-08','Kewarganegaraan','I',NULL,NULL,'Hadir'),
(49,4,'1931733052','Devalia Puteri','MI','2021-07-08','Kewarganegaraan','I',3,NULL,'Alpha'),
(54,18,'1931733045','Mark','MI','2021-07-17','Desain Grafis','A',4,'1931733045-18-16265017791861376590.jpeg','Acara Keluarga');

/*Table structure for table `dosen` */

DROP TABLE IF EXISTS `dosen`;

CREATE TABLE `dosen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nidn` varchar(20) NOT NULL,
  `nama_dosen` varchar(50) DEFAULT NULL,
  `jenis_kelamin` varchar(12) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `nama_matkul` varchar(50) DEFAULT NULL,
  `semester` varchar(20) DEFAULT NULL,
  `tingkat` varchar(20) DEFAULT NULL,
  `nama_kelas` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`nidn`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `dosen` */

insert  into `dosen`(`id`,`nidn`,`nama_dosen`,`jenis_kelamin`,`hp`,`nama_matkul`,`semester`,`tingkat`,`nama_kelas`,`updated_at`,`created_at`) values 
(1,'0701078302','Andika Kurnia Adi P., S. Kom., M.Cs.','Laki-Laki','085543345543','10','2','3','11','2021-07-07 20:42:51','2021-07-07 20:42:51'),
(16,'0709128502','Agustono Heriadi, S.ST.,M.Kom.','Laki-Laki','085123098897','12','2','2','9','2021-07-07 22:06:17','2021-07-07 22:06:17'),
(15,'0710049004','Rinanza Zulmy Alhamri, S.Kom., M.Kom.','Laki-Laki','085654456123','4','2','2','8','2021-07-07 22:04:13','2021-07-07 22:04:13'),
(9,'0711108704','Toga Aldila C., S. ST., M. Sc.','Laki-Laki','085567765434','9','2','3','15','2021-07-07 00:00:00','2021-07-04 18:38:48'),
(18,'0720012345','Fikha Rizky Aullia, S.Pd., M.Pd.','Perempuan','085111543345','2','2','1','3','2021-07-07 00:00:00','2021-07-07 22:11:07'),
(20,'0720054321','Imam Junaedi, S.T., M.T','Laki-Laki','085987654222','7','2','1','5','2021-07-07 22:14:42','2021-07-07 22:14:42'),
(8,'0720097801','Fery Sofian Efendi, S. Kom., M.Cs.','Laki-Laki','085123321333','5','2','3','3','2021-07-07 00:00:00','2021-06-23 19:30:00'),
(2,'0722079101','Selvia Ferdiana Kusuma, S.Kom., M.Kom.','Perempuan','085676656657','16','2','1','1','2021-07-07 11:48:26','2021-07-07 11:48:26'),
(12,'0724068102','Benni Agung Nugroho, S. Kom., M.Cs.','Laki-Laki','085987789999','18','2','3','14','2021-07-07 00:00:00','2021-07-07 21:49:07'),
(14,'0728058902','Kunti Elliyen,S.Kom., M.T.','Perempuan','085098890999','1','2','2','7','2021-07-07 22:03:07','2021-07-07 22:03:07'),
(13,'0728109001','Abidatul Izzah, S.Si., M. Kom.','Perempuan','085876678888','8','2','2','6','2021-07-07 00:00:00','2021-07-07 22:01:35'),
(19,'0729900999','Irfin Sandra Asti, S.S.T.,M.T.','Perempuan','085888765567','3','2','1','4','2021-07-07 22:12:32','2021-07-07 22:12:32'),
(17,'0730038201','Fadelis Sukya, S. Kom, M. Cs.','Laki-Laki','085777666444','17','2','2','10','2021-07-07 00:00:00','2021-07-07 22:07:32'),
(11,'0730048901','Ratna Widyastuti, S.Pd., M. Pd.','Perempuan','085567765555','11','2','3','12','2021-07-07 00:00:00','2021-06-24 13:05:36'),
(3,'0730068004','Ellya Nurfarida, S.T., M.Cs.','Perempuan','085123321111','15','2','1','2','2021-07-07 00:00:00','2021-07-07 11:45:51'),
(21,'1990013','Aldi','Laki-Laki','04566','3','2','1','1','2021-07-11 21:43:17','2021-07-11 21:43:17');

/*Table structure for table `jadwal` */

DROP TABLE IF EXISTS `jadwal`;

CREATE TABLE `jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `id_semester` int(11) NOT NULL,
  `id_tingkat` int(20) DEFAULT NULL,
  `id_kelas` int(11) NOT NULL,
  `nidn` varchar(20) DEFAULT NULL,
  `kode_matkul` int(20) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `nama_dosen` varchar(50) DEFAULT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `int_hari` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `jadwal` */

insert  into `jadwal`(`id_jadwal`,`id_semester`,`id_tingkat`,`id_kelas`,`nidn`,`kode_matkul`,`hari`,`nama_dosen`,`jam_mulai`,`jam_selesai`,`updated_at`,`created_at`,`int_hari`) values 
(1,2,1,1,'722079101',16,'Kamis','Selvia Ferdiana Kusuma, S.Kom., M.Kom.','20:00:00','23:00:00','2021-07-07 00:00:00','2021-07-07 21:54:25',5),
(2,2,1,2,NULL,15,'Selasa','Ellya Nurfarida, S.T., M.Cs.','08:00:00','11:20:00','2021-07-07 22:20:46','2021-07-07 22:20:46',3),
(3,2,1,3,'720012345',2,'Rabu','Fikha Rizky Aullia, S.Pd., M.Pd.','08:00:00','11:20:00','2021-07-07 00:00:00','2021-07-07 22:21:49',4),
(4,2,1,4,NULL,3,'Kamis','Irfin Sandra Asti, S.S.T.,M.T.','08:00:00','10:20:00','2021-07-07 22:22:35','2021-07-07 22:22:35',5),
(5,2,1,5,NULL,7,'Kamis','Imam Junaedi, S.T., M.T','08:00:00','12:10:00','2021-07-07 22:24:58','2021-07-07 22:24:58',5),
(6,2,2,6,NULL,8,'Senin','Abidatul Izzah, S.Si., M. Kom.','08:00:00','13:00:00','2021-07-07 22:40:37','2021-07-07 22:40:37',2),
(7,2,2,7,NULL,1,'Selasa','Kunti Elliyen,S.Kom., M.T.','08:00:00','11:20:00','2021-07-07 22:41:47','2021-07-07 22:41:47',3),
(8,2,2,8,NULL,4,'Rabu','Rinanza Zulmy Alhamri, S.Kom., M.Kom.','08:00:00','13:00:00','2021-07-07 22:42:35','2021-07-07 22:42:35',4),
(9,2,2,9,NULL,12,'Kamis','Agustono Heriadi, S.ST.,M.Kom.','08:00:00','11:20:00','2021-07-07 22:43:30','2021-07-07 22:43:30',5),
(10,2,2,10,'0730038201',17,'Jumat','Fadelis Sukya, S. Kom, M. Cs.','08:00:00','11:20:00','2021-07-08 00:00:00','2021-07-07 22:44:19',6),
(11,2,3,11,NULL,10,'Senin','Andika Kurnia Adi P., S. Kom., M.Cs.','08:00:00','13:00:00','2021-07-07 22:45:23','2021-07-07 22:45:23',2),
(12,2,3,12,NULL,11,'Jumat','Ratna Widyastuti, S.Pd., M. Pd.','08:00:00','11:20:00','2021-07-07 22:46:12','2021-07-07 22:46:12',6),
(13,2,2,13,NULL,5,'Rabu','Fery Sofian Efendi, S. Kom., M.Cs.','08:00:00','11:20:00','2021-07-07 22:47:34','2021-07-07 22:47:34',4),
(14,2,3,14,'0724068102',18,'Selasa','Benni Agung Nugroho, S. Kom., M.Cs.','08:00:00','14:40:00','2021-07-07 00:00:00','2021-07-07 22:49:02',3),
(15,2,3,15,NULL,9,'Kamis','Toga Aldila C., S. ST., M. Sc.','08:00:00','13:00:00','2021-07-07 22:51:24','2021-07-07 22:51:24',5),
(16,1,1,1,NULL,8,'Kamis','Rinanza Zulmy Alhamri, S.Kom., M.Kom.','08:00:00','11:00:00','2021-07-08 11:28:28','2021-07-08 11:28:28',5),
(17,1,1,2,NULL,4,'Kamis','Ratna Widyastuti, S.Pd., M. Pd.','09:30:00','00:00:00','2021-07-08 11:31:39','2021-07-08 11:31:39',5),
(18,1,1,1,'0720097801',5,'Sabtu','Fery Sofian Efendi, S. Kom., M.Cs.','12:00:00','15:00:00','2021-07-08 11:35:03','2021-07-08 11:35:03',7),
(19,2,1,1,'0729900999',4,'Kamis','Irfin Sandra Asti, S.S.T.,M.T.','08:00:00','12:00:00','2021-07-12 00:00:00','2021-07-12 11:06:26',5),
(20,2,1,1,'0701078302',8,'Selasa','Andika Kurnia Adi P., S. Kom., M.Cs.','08:00:00','12:00:00','2021-07-13 13:32:11','2021-07-13 13:32:11',3),
(21,2,1,2,'0701078302',9,'Selasa','Andika Kurnia Adi P., S. Kom., M.Cs.','08:00:00','10:00:00','2021-07-13 13:32:39','2021-07-13 13:32:39',3),
(24,2,1,3,'0709128502',8,'Rabu','Agustono Heriadi, S.ST.,M.Kom.','08:00:00','11:00:00','2021-07-13 14:16:00','2021-07-13 14:16:00',4),
(25,2,1,3,'0709128502',8,'Kamis','Agustono Heriadi, S.ST.,M.Kom.','08:00:00','11:00:00','2021-07-13 14:16:29','2021-07-13 14:16:29',5);

/*Table structure for table `jam` */

DROP TABLE IF EXISTS `jam`;

CREATE TABLE `jam` (
  `id_jam` int(11) NOT NULL AUTO_INCREMENT,
  `jam` varchar(10) DEFAULT NULL,
  `mulai` time DEFAULT NULL,
  `selesai` time DEFAULT NULL,
  PRIMARY KEY (`id_jam`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `jam` */

insert  into `jam`(`id_jam`,`jam`,`mulai`,`selesai`) values 
(1,'1','07:30:00','10:00:00');

/*Table structure for table `kelas` */

DROP TABLE IF EXISTS `kelas`;

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelas` varchar(15) NOT NULL,
  `semester` varchar(30) DEFAULT NULL,
  `tingkat` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `kelas` */

insert  into `kelas`(`id_kelas`,`nama_kelas`,`semester`,`tingkat`,`updated_at`,`created_at`) values 
(1,'A','2','1','2021-07-07 11:37:36','2021-07-07 11:37:36'),
(2,'B','2','1','2021-07-07 11:38:31','2021-07-07 11:38:31'),
(3,'C','2','1','2021-07-07 00:00:00','2021-07-07 19:24:45'),
(4,'D','2','1','2021-07-07 19:25:09','2021-07-07 19:25:09'),
(5,'E','2','1','2021-07-07 19:37:19','2021-07-07 19:37:19'),
(6,'A','2','2','2021-07-07 19:37:31','2021-07-07 19:37:31'),
(7,'B','2','2','2021-07-07 19:38:38','2021-07-07 19:38:38'),
(8,'C','2','2','2021-07-07 19:39:33','2021-07-07 19:39:33'),
(9,'D','2','2','2021-07-07 19:39:46','2021-07-07 19:39:46'),
(10,'E','2','2','2021-07-07 19:40:15','2021-07-07 19:40:15'),
(11,'A','2','3','2021-07-07 19:40:52','2021-07-07 19:40:52'),
(12,'B','2','3','2021-07-07 00:00:00','2021-07-07 19:41:09'),
(13,'C','2','3','2021-07-07 19:41:42','2021-07-07 19:41:42'),
(14,'D','2','3','2021-07-07 19:41:56','2021-07-07 19:41:56'),
(15,'E','2','3','2021-07-07 19:42:09','2021-07-07 19:42:09');

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_semester` int(11) DEFAULT NULL,
  `nim` varchar(10) DEFAULT NULL,
  `nama_mhs` varchar(50) DEFAULT NULL,
  `prodi` varchar(30) DEFAULT NULL,
  `semester` varchar(2) DEFAULT NULL,
  `ttl` varchar(30) DEFAULT NULL,
  `jenis_kelamin` varchar(12) DEFAULT NULL,
  `alamat_lengkap` longtext DEFAULT NULL,
  `agama` varchar(12) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`id`,`user_id`,`id_kelas`,`id_semester`,`nim`,`nama_mhs`,`prodi`,`semester`,`ttl`,`jenis_kelamin`,`alamat_lengkap`,`agama`,`updated_at`,`created_at`) values 
(11,6,1,2,'1931733045','Mark','MI','1','Kediri','Laki-Laki','Gurah','Islam','2021-07-07 00:00:00','2021-06-15 21:52:41'),
(17,17,1,2,'1931733090','Anjasmara','MI','1','Kediri, 4 Desember 2000','Laki-Laki','Klaten','Islam','2021-07-07 00:00:00','2021-07-04 06:33:48'),
(18,18,14,2,'1931733075','Ika Puspita Sari','MI','1','Blitar, 22 Agustus 2000',NULL,'Blitar','Islam','2021-07-07 00:00:00','2021-07-06 22:11:42'),
(19,19,14,2,'1931733001','Agysta Wahyu','MI','1','Malang, 13 Maret 1999',NULL,'Malang','Islam','2021-07-07 00:00:00','2021-07-07 09:36:12'),
(20,20,14,2,'1931733107','Dendy Fajar P','MI','2','Kediri, 13 April 2020',NULL,'Kediri','Islam','2021-07-07 00:00:00','2021-07-07 19:34:40'),
(21,21,14,2,'1931733076','Alisia Firda S','MI','2','Kediri, 22 Juli 1999','Perempuan','Kediri','Islam','2021-07-07 19:48:38','2021-07-07 19:48:38'),
(22,22,14,2,'1931733092','Fernanda Eka N','MI','2','Kediri, 29 Februari 2000','Perempuan','Kediri','Islam','2021-07-07 19:50:42','2021-07-07 19:50:42'),
(23,23,14,2,'1931733093','Isna Uswatun K','MI','2','Kediri, 08 Desember 1999','Perempuan','Kediri','Islam','2021-07-07 19:52:06','2021-07-07 19:52:06'),
(24,24,14,2,'1931733082','Virginia Abrinsa P','MI','2','Kediri, 23 September 1999','Perempuan','Gurah','Islam','2021-07-07 19:54:38','2021-07-07 19:54:38'),
(25,25,14,2,'1931733102','Wildan Azyumardi A','MI','2','Kalimantan, 14 Mei 2000','Laki-Laki','Wates','Islam','2021-07-07 00:00:00','2021-07-07 19:55:57'),
(26,26,14,2,'1931733103','Mahfud Adi S','MI','2','Madiun, 11 Maret 2000','Laki-Laki','Madiun','Islam','2021-07-07 19:57:03','2021-07-07 19:57:03'),
(27,27,14,2,'1931733101','Ahmad Setyo Anggoro','MI','2','Nganjuk, 25 April 1999','Laki-Laki','Nganjuk','Islam','2021-07-07 19:58:41','2021-07-07 19:58:41'),
(28,28,15,2,'1931733055','Yeni Rahayu','MI','2','Kediri, 19 Januari 2000','Perempuan','Kediri','Islam','2021-07-07 20:00:01','2021-07-07 20:00:01'),
(29,29,15,2,'1931733056','Ratna Dwi','MI','2','Kediri, 25 November 1999','Perempuan','Bujel','Islam','2021-07-07 20:01:38','2021-07-07 20:01:38'),
(30,30,15,2,'1931733057','Fikri Nanda','MI','2','Kediri, 28 Juni 1999','Laki-Laki','Sukorame','Islam','2021-07-07 20:02:55','2021-07-07 20:02:55'),
(31,31,15,2,'1931733058','Fanny Bagus','MI','2','Kediri, 12 September 1999','Laki-Laki','Ngasem','Islam','2021-07-07 20:03:53','2021-07-07 20:03:53'),
(32,32,15,2,'1931733059','Rival Ardiansyah','MI','2','Kediri, 05 Mei 1999','Laki-Laki','Mojoroto','Islam','2021-07-07 20:05:23','2021-07-07 20:05:23'),
(33,33,15,2,'1931733060','Ayu Fakta','MI','2','Kediri, 03 Maret 1999','Perempuan','Mojoroto','Islam','2021-07-07 20:07:01','2021-07-07 20:07:01'),
(34,34,15,2,'1931733061','Hafidi Muslim','MI','2','Nganjuk, 19 April 1999','Laki-Laki','Nganjuk','Islam','2021-07-07 20:08:20','2021-07-07 20:08:20'),
(35,35,15,2,'1931733062','Qori Wihajar N','MI','2','Tulungagung, 27 September 1999','Perempuan','Tulungagung','Islam','2021-07-07 20:09:25','2021-07-07 20:09:25'),
(36,36,15,2,'1931733063','Ihtiara Afrisani','MI','2','Blitar, 6 Februari 1999','Perempuan','Blitar','Islam','2021-07-07 20:10:24','2021-07-07 20:10:24'),
(37,37,15,2,'1931733064','Vindi Dwi','MI','2','Trenggalek, 8 Mei 1999','Perempuan','Trenggalek','Islam','2021-07-07 20:12:08','2021-07-07 20:12:08'),
(38,38,11,2,'1931733065','Rizqi Bungsu','MI','2','Kediri, 17 Oktober 1999','Perempuan','Kandat','Islam','2021-07-07 00:00:00','2021-07-07 20:13:24'),
(39,39,11,2,'1931733066','Efendi Mulya','MI','2','Kediri, 9 September 1999','Laki-Laki','Ngasem','Islam','2021-07-07 20:14:58','2021-07-07 20:14:58'),
(40,40,11,2,'1931733067','Salma Aulia','MI','2','Bali, 12 Desember 2000','Perempuan','Kediri','Islam','2021-07-07 20:16:23','2021-07-07 20:16:23'),
(41,41,11,2,'1931733068','Niko Irham','MI','2','Bandung, 18 Agustus 1998','Laki-Laki','Kediri','Islam','2021-07-07 20:17:40','2021-07-07 20:17:40'),
(42,42,11,2,'1931733069','Renata Kusuma','MI','2','Malang, 5 Juni 1999','Perempuan','Malang','Islam','2021-07-07 20:20:50','2021-07-07 20:20:50'),
(43,43,11,2,'1931733070','Abimanyu','MI','2','Bandung, 19 November 1999','Laki-Laki','Kediri','Islam','2021-07-07 20:21:57','2021-07-07 20:21:57'),
(44,44,11,2,'1931733071','Benita Della','MI','2','Bali, 22 Juni 1998','Perempuan','Mojoroto','Islam','2021-07-07 20:23:13','2021-07-07 20:23:13'),
(45,45,11,2,'1931733072','Faria Qoriatul','MI','2','Tangerang, 25 Februari 1998','Perempuan','Kediri','Islam','2021-07-07 20:24:15','2021-07-07 20:24:15'),
(46,46,11,2,'1931733073','Ananta Darma','MI','2','Tangerang, 26 Mei 1999','Laki-Laki','Kediri','Islam','2021-07-07 20:26:14','2021-07-07 20:26:14'),
(47,47,11,2,'1931733074','Jefri Nichol','MI','2','Bandung, 13 Agustus 1999','Laki-Laki','Malang','Islam','2021-07-07 20:27:20','2021-07-07 20:27:20'),
(48,48,12,2,'1931733077','Angga Yunanda','MI','2','Tangerang, 22 September 1998','Laki-Laki','Semarang','Islam','2021-07-07 20:29:57','2021-07-07 20:29:57'),
(49,49,12,2,'1931733078','Renata Putri','MI','2','Kebumen, 16 Agustus 1998','Perempuan','Kediri','Islam','2021-07-07 20:31:17','2021-07-07 20:31:17'),
(50,50,12,2,'1931733079','Sukma Fauziah','MI','2','Gresik, 13 April 1999','Perempuan','Gresik','Islam','2021-07-07 20:32:22','2021-07-07 20:32:22'),
(51,51,12,2,'1931733080','Boy William','MI','2','Jakarta, 12 Februari 1998',NULL,'Kertosono','Islam','2021-07-07 00:00:00','2021-07-07 20:33:38'),
(52,52,12,2,'1931733081','Devi Maharani','MI','2','Kediri, 3 Maret 1999','Perempuan','Kediri','Islam','2021-07-07 00:00:00','2021-07-07 20:45:22'),
(53,53,12,2,'1931733082','Aqilla Adjrah','MI','2','Medan, 17 September 1999','Perempuan','Kediri','Islam','2021-07-07 20:46:57','2021-07-07 20:46:57'),
(54,54,12,2,'1931733083','Rina Diana','MI','2','Bandung, 16 Agustus 1999','Perempuan','Kediri','Islam','2021-07-07 00:00:00','2021-07-07 20:48:00'),
(55,55,12,2,'1931733084','Riko Lesmana','MI','2','Surabaya, 13 April 1999','Laki-Laki','Jombang','Islam','2021-07-07 20:49:27','2021-07-07 20:49:27'),
(56,56,12,2,'1931733085','Shofi Romal Izzul','MI','2','Blitar, 27 Februari 1999','Laki-Laki','Blitar','Islam','2021-07-07 20:50:38','2021-07-07 20:50:38'),
(57,57,12,2,'1931733086','Afrina Suria','MI','2','Kediri, 12 Januari 1999','Perempuan','Gampengrejo','Islam','2021-07-07 20:51:30','2021-07-07 20:51:30'),
(58,58,13,2,'1931733087','Michelle Joana','MI','2','Surabaya, 19 Oktober 1999','Perempuan','Kediri','Islam','2021-07-07 20:56:51','2021-07-07 20:56:51'),
(59,59,13,2,'1931733088','Yanuar Dicky','MI','2','Kediri, 11 Januari 1999','Laki-Laki','Kediri','Islam','2021-07-07 00:00:00','2021-07-07 20:57:47'),
(60,60,13,2,'1931733089','Satria Yuda','MI','2','Surabaya, 20 Juni 1999','Laki-Laki','Surabaya','Islam','2021-07-07 20:59:03','2021-07-07 20:59:03'),
(61,61,13,2,'1931733090','Aulya Poppy','MI','2','Semarang, 23 Juli 1999','Perempuan','Jombang','Islam','2021-07-07 21:00:45','2021-07-07 21:00:45'),
(62,62,13,2,'1931733091','Yahya Fatchurrahman','MI','2','Surakarta, 24 Agustus 1998','Laki-Laki','Surabaya','Islam','2021-07-07 21:01:46','2021-07-07 21:01:46'),
(63,63,13,2,'1931733094','Husein Ahmad','MI','2','Surabaya, 18 Mei 2000','Laki-Laki','Gresik','Islam','2021-07-07 21:03:03','2021-07-07 21:03:03'),
(64,64,13,2,'1931733095','Jelita Aprilia','MI','2','Madiun, 13 Mei 1999','Perempuan','Kertosono','Islam','2021-07-07 21:04:08','2021-07-07 21:04:08'),
(65,65,13,2,'1931733096','Momo Aulya','MI','2','Gresik, 19 April 1999','Perempuan','Gresik','Islam','2021-07-07 21:05:36','2021-07-07 21:05:36'),
(66,66,13,2,'1931733097','Yahya Purnama','MI','2','Kediri, 1 Oktober 1999','Laki-Laki','Kediri','Islam','2021-07-07 21:06:44','2021-07-07 21:06:44'),
(67,67,13,2,'1931733098','Endang Sulastri N','MI','2','Kediri, 2 Oktober 1999','Perempuan','Kediri','Islam','2021-07-07 21:07:42','2021-07-07 21:07:42'),
(68,68,1,2,'1931733046','Ina Marinka','MI','2','Kediri, 14 Februari 1999','Perempuan','Malang','Islam','2021-07-07 21:10:25','2021-07-07 21:10:25'),
(69,69,1,2,'1931733047','Stella Okta','MI','2','Semarang, 12 April 1999','Perempuan','Kediri','Islam','2021-07-07 21:11:50','2021-07-07 21:11:50'),
(70,70,2,2,'1931733048','Belly Aprillia','MI','2','Kediri, 12 April 1999','Perempuan','Kediri','Islam','2021-07-07 00:00:00','2021-07-07 21:12:50'),
(71,71,2,2,'1931733049','Brandon Salim','MI','2','Bandung, 11 Januari 1999','Laki-Laki','Kediri','Islam','2021-07-07 21:14:05','2021-07-07 21:14:05'),
(72,72,3,2,'1931733050','Cristya Putri','MI','2','Surabaya, 19 Mei 1999','Perempuan','Surabaya','Islam','2021-07-07 21:15:20','2021-07-07 21:15:20'),
(73,73,3,2,'1931733051','Cornelia Putri','MI','2','Surabaya, 11 Mei 1999','Perempuan','Surabaya','Islam','2021-07-07 21:16:25','2021-07-07 21:16:25'),
(74,74,4,2,'1931733052','Devalia Puteri','MI','2','Surabaya, 11 Januari 1999','Perempuan','Surabaya','Islam','2021-07-07 22:55:40','2021-07-07 22:55:40'),
(75,75,4,2,'1931733053','Devano Putra','MI','2','Bandung, 9 Desember 1999','Laki-Laki','Kediri','Islam','2021-07-07 22:56:34','2021-07-07 22:56:34'),
(76,76,5,2,'1931733104','Elvin Wicaksana','MI','2','Jakarta, 19 Mei 1999','Laki-Laki','Kediri','Islam','2021-07-07 23:03:23','2021-07-07 23:03:23'),
(77,77,5,2,'1931733105','Evi Sinta','MI','2','Surabaya, 7 Maret 1999','Perempuan','Kediri','Islam','2021-07-07 23:04:24','2021-07-07 23:04:24');

/*Table structure for table `matkul` */

DROP TABLE IF EXISTS `matkul`;

CREATE TABLE `matkul` (
  `kode_matkul` int(20) NOT NULL AUTO_INCREMENT,
  `nama_matkul` varchar(50) DEFAULT NULL,
  `sks` varchar(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`kode_matkul`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `matkul` */

insert  into `matkul`(`kode_matkul`,`nama_matkul`,`sks`,`updated_at`,`created_at`) values 
(1,'B.Indonesia','4',NULL,NULL),
(2,'B.Inggris','4',NULL,NULL),
(3,'Kewarganegaraan','4','2021-07-07 00:00:00',NULL),
(4,'K3','6','2021-05-05 01:11:43','2021-05-05 01:11:43'),
(5,'Desain Grafis','4','2021-06-17 21:22:44','2021-06-17 21:22:44'),
(7,'Desain Antarmuka','5','2021-07-07 00:00:00','2021-06-23 14:38:32'),
(8,'PBO','6','2021-07-07 00:00:00','2021-07-07 09:22:24'),
(9,'Jaringan Komputer','6','2021-07-07 00:00:00','2021-07-07 09:22:49'),
(10,'Basis Data Lanjut','6','2021-07-07 09:23:30','2021-07-07 09:23:30'),
(11,'Etika Profesi Bidang TI','4','2021-07-07 09:24:05','2021-07-07 09:24:05'),
(12,'Sistem Informasi','4','2021-07-07 09:24:47','2021-07-07 09:24:47'),
(13,'Struktur Data','8','2021-07-07 09:25:50','2021-07-07 09:25:50'),
(14,'Sistem Operasi','8','2021-07-07 09:26:04','2021-07-07 09:26:04'),
(15,'Ilmu Komunikasi','4','2021-07-07 09:26:40','2021-07-07 09:26:40'),
(16,'Dasar Pemrograman','8','2021-07-07 09:26:54','2021-07-07 09:26:54'),
(17,'Basis Data','4','2021-07-07 22:33:33','2021-07-07 22:33:33'),
(18,'Pemrograman Mobile','8','2021-07-07 22:35:59','2021-07-07 22:35:59');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

/*Table structure for table `semester` */

DROP TABLE IF EXISTS `semester`;

CREATE TABLE `semester` (
  `id_semester` int(11) NOT NULL AUTO_INCREMENT,
  `semester` varchar(30) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_semester`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `semester` */

insert  into `semester`(`id_semester`,`semester`,`updated_at`,`created_at`) values 
(1,'Ganjil 2020/2021','2021-07-07 11:06:17','2021-07-07 11:06:17'),
(2,'Genap 2020/2021','2021-07-07 11:07:05','2021-07-07 11:07:05');

/*Table structure for table `tingkat` */

DROP TABLE IF EXISTS `tingkat`;

CREATE TABLE `tingkat` (
  `id_tingkat` int(20) NOT NULL AUTO_INCREMENT,
  `tingkat` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_tingkat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tingkat` */

insert  into `tingkat`(`id_tingkat`,`tingkat`) values 
(1,'I (Satu)'),
(2,'II (Dua)'),
(3,'III (Tiga)');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `level` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mahasiswa_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`level`,`name`,`username`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`,`mahasiswa_id`) values 
(10,'admin','firda safira','safira','s@gmail',NULL,'$2y$10$Uyqj1Cvl87KYvWB3cjJGD.uyZkAtBCa1pyuaQyqFa.Aqy2EM6rgJu',NULL,'2021-06-05 06:39:10','2021-06-07 00:00:00',NULL),
(13,'admin','firda','firday','f@gmail',NULL,'$2y$10$YqAOvVqKVD14YITjAzHMN.UWj5bfmzbaLYHGp4cTP8qTjYDQR6WyW',NULL,NULL,'2021-06-07 00:00:00',NULL),
(14,'admin','ITSTAFF','itstaff','itstaff@mail.com',NULL,'$2y$10$pVmPyK7e.9g.YbKFGbZhve9zAeiu45X4c0ebtsX8hYVDemtQOIcQO',NULL,'2021-06-09 19:57:28','2021-07-06 21:14:16',NULL),
(15,'mahasiswa','MARK','mark','mark@mail.com',NULL,'$2y$10$u.cM/6OxAwisZM5FtvqKJ.Bpa50n5mAZUP1n1ydVF5a2FNo76B9QW',NULL,'2021-06-09 19:57:32','2021-06-29 22:40:52',6),
(17,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(24,'mahasiswa','Anjas','1931733090','anjas@mail.com',NULL,'$2y$10$yALgYmjuDNo43gHVQZAlCeTGfujJ8vQNBQ8PsRAvlPXqCBDJ3fDQe',NULL,'2021-07-04 06:33:48','2021-07-04 06:33:48',17),
(27,'mahasiswa','Ika Puspita Sari','1931733075','ika@gmail.com',NULL,'$2y$10$AYmthUpcrCvliC/PVc9LtOBwOYV5zS.CJ9NvgReHqbDhm8lwTg9Em',NULL,'2021-07-06 22:11:42','2021-07-06 22:11:42',18),
(28,'mahasiswa','Agysta Wahyu','1931733001','agysta@gmail.com',NULL,'$2y$10$GEIi5BjcOLvCW3dO4vNRxuB/VfkQE6ycefgc.6Cn8ujW/dZO.0hli',NULL,'2021-07-07 09:36:12','2021-07-07 09:36:12',19),
(29,'mahasiswa','Dendy Fajar P','1931733107','dendy@gmail.com',NULL,'$2y$10$r26hAUaVwJUVViuPjzpXuuBwyPlNhmoufol2gFNdcqUASYduLWkE2',NULL,'2021-07-07 19:34:40','2021-07-07 19:34:40',20),
(30,'mahasiswa','Alisia Firda S','1931733076','firda@gmail.com',NULL,'$2y$10$ZfxoUWMM2uGffliKkDYyYecBd6pd5l4Zp.Ez6QEFnp.Y0Iu9zS6Li',NULL,'2021-07-07 19:48:38','2021-07-07 19:48:38',21),
(31,'mahasiswa','Fernanda Eka N','1931733092','fernanda@gmail.com',NULL,'$2y$10$HBJBlLYLtG4Mg7r0gtD7tO7RmdFhbn.SVqpeMIF2AZF9./p.Z2Y6u',NULL,'2021-07-07 19:50:42','2021-07-07 19:50:42',22),
(32,'mahasiswa','Isna Uswatun K','1931733093','isna@gmail.com',NULL,'$2y$10$21zCkTdMeRnLMdxi6q2WoOuMu9reBd/ZtbPnJUdwfDN7mBtpIybwS',NULL,'2021-07-07 19:52:06','2021-07-07 19:52:06',23),
(33,'mahasiswa','Virginia Abrinsa P','1931733082','virginia@gmail.com',NULL,'$2y$10$.ra8Y3hOL5YLjkWGvHrEyOe3luAQPl/9HZbXSYr66kSs/cfSkAMG2',NULL,'2021-07-07 19:54:39','2021-07-07 19:54:39',24),
(34,'mahasiswa','Wildan Azyumardi A','1931733102','wildan@gmail.com',NULL,'$2y$10$csZkQu/JquWKkb4znotv2.SdYKzWoo1HC5wYCp/3X38.vfF7aF8HS',NULL,'2021-07-07 19:55:57','2021-07-07 19:55:57',25),
(35,'mahasiswa','Mahfud Adi S','1931733103','mahfud@gmail.com',NULL,'$2y$10$3owu2bpwhK4JgSGyVKKu4OGqKWLLou5mshQg5X7CUflXuEy2CJ.ya',NULL,'2021-07-07 19:57:03','2021-07-07 19:57:03',26),
(36,'mahasiswa','Ahmad Setyo Anggoro','1931733101','ahmad@gmail.com',NULL,'$2y$10$Xbkibbb4YrpbaTjwfKahh.x7esZYdfYDXB6Rjo9M64iPE9BpT0ZSO',NULL,'2021-07-07 19:58:41','2021-07-07 19:58:41',27),
(37,'mahasiswa','Yeni Rahayu','1931733055','yeni@gmail.com',NULL,'$2y$10$2dtPiOdmWit9F9pGPkK2k.w1zAtekN2S81Gg87X5ZCJ7fUraVv8EK',NULL,'2021-07-07 20:00:01','2021-07-07 20:00:01',28),
(38,'mahasiswa','Ratna Dwi','1931733056','ratna@gmail.com',NULL,'$2y$10$R/IVMLVhhd2ZQa/Sm470h.gMzNoi9/QOXGFtg4IsOAjpEba2RcAmO',NULL,'2021-07-07 20:01:38','2021-07-07 20:01:38',29),
(39,'mahasiswa','Fikri Nanda','1931733057','fikri@gmail.com',NULL,'$2y$10$fq.61hmyj4U4JxCzlPYtIOA0Mwp9R.V923yACGuxAU8w9TE0X/Xd2',NULL,'2021-07-07 20:02:55','2021-07-07 20:02:55',30),
(40,'mahasiswa','Fanny Bagus','1931733058','fanny@gmail.com',NULL,'$2y$10$94NwlZikM6.yqAJJogQrpu3slWNt63g/vgWkKtV/uL0BUVXbGYE4i',NULL,'2021-07-07 20:03:54','2021-07-07 20:03:54',31),
(41,'mahasiswa','Rival Ardiansyah','1931733059','rival@gmail.com',NULL,'$2y$10$3rv2WlTlIQFG0y4mxtsa1e3VwU8miS6n3r/mS7qfBLoFPGZbz9Bnu',NULL,'2021-07-07 20:05:23','2021-07-07 20:05:23',32),
(42,'mahasiswa','Ayu Fakta','1931733060','ayu@gmail.com',NULL,'$2y$10$1oN7WoQnp2zMFfWPAIgqxOexy6bdt8WPSayq8UrPISpvJoL3Gq.KW',NULL,'2021-07-07 20:07:02','2021-07-07 20:07:02',33),
(43,'mahasiswa','Hafidi Muslim','1931733061','hafid@gmail.com',NULL,'$2y$10$FfpGiZboqbN0NmlMev4Xde.Aq4kBLRBLb8yLw0cGHiP4VA59kTI0G',NULL,'2021-07-07 20:08:21','2021-07-07 20:08:21',34),
(44,'mahasiswa','Qori Wihajar N','1931733062','qori@gmail.com',NULL,'$2y$10$rxsbpdFDhS0JEGIWO0B5NuHhQ6AbQm7nz4TA1OsXoJa.uCK/chnL.',NULL,'2021-07-07 20:09:25','2021-07-07 20:09:25',35),
(45,'mahasiswa','Ihtiara Afrisani','1931733063','ih@gmail.com',NULL,'$2y$10$zR6bNzcs7w9CfcM6w/NOWu3aGhno/UiASeqY/y9HbWIOUGgB/Qu1y',NULL,'2021-07-07 20:10:24','2021-07-07 20:10:24',36),
(46,'mahasiswa','Vindi Dwi','1931733064','vindi@gmail.com',NULL,'$2y$10$aVYIJhd11W1nyePTlzqgHObY7.xDlnTd0F.FS23vY03s2VHl67sXC',NULL,'2021-07-07 20:12:09','2021-07-07 20:12:09',37),
(47,'mahasiswa','Rizqi Bungsu','1931733064','bungsu@gmail.com',NULL,'$2y$10$2ds3rgeNS5mGsYE3IQZdfOq/lPhAQrtGcmwX2HeyGxcR/1284V4xC',NULL,'2021-07-07 20:13:24','2021-07-07 20:13:24',38),
(48,'mahasiswa','Efendi Mulya','1931733066','pundi@gmail.com',NULL,'$2y$10$22sX3yuvPpbzMXZKL0eCSu7gIbsPf4Vx8X4RoDQnLBjrzAjsGwpWa',NULL,'2021-07-07 20:14:58','2021-07-07 20:14:58',39),
(49,'mahasiswa','Salma Aulia','1931733067','salma@gmail.com',NULL,'$2y$10$Nsr7rRXlhI/zik763vLLb.DmgcJDwf0TiTOVMiK2N.emgBEVOGj.W',NULL,'2021-07-07 20:16:24','2021-07-07 20:16:24',40),
(50,'mahasiswa','Niko Irham','1931733068','niko@gmail.com',NULL,'$2y$10$JQdBVgGku0eD2BTd3KB2KOLNvD4DG8cmHjyqnl8B0TSKhuhtO5w4C',NULL,'2021-07-07 20:17:40','2021-07-07 20:17:40',41),
(51,'mahasiswa','Renata Kusuma','1931733069','rena@gmail.com',NULL,'$2y$10$CYi5aSFuQe2m1Yj.ZX6vler5ZnE5ylzvq/xr9wRQWvzuYBvTanpde',NULL,'2021-07-07 20:20:50','2021-07-07 20:20:50',42),
(52,'mahasiswa','Abimanyu','1931733070','abim@gmail.com',NULL,'$2y$10$PvhQp6yXMyPO/cPm7cweYuPGVx.5LaSxw3koBhaAqUEYDXQdelz2m',NULL,'2021-07-07 20:21:58','2021-07-07 20:21:58',43),
(53,'mahasiswa','Benita Della','1931733071','della@gmail.com',NULL,'$2y$10$wgzjZziew5PjNbPcSYcBqedvDh5mrL8ThfBHuez.corFoaZsWYGue',NULL,'2021-07-07 20:23:13','2021-07-07 20:23:13',44),
(54,'mahasiswa','Faria Qoriatul','1931733072','far@gmail.com',NULL,'$2y$10$ohKorKfTgYRbx/eOKwbqOutbvwWu7.6xazVBgptY2xYzCKKI9/vum',NULL,'2021-07-07 20:24:16','2021-07-07 20:24:16',45),
(55,'mahasiswa','Ananta Darma','1931733073','nanta@gmail.com',NULL,'$2y$10$/KxfaWYJI0QVdFlm8al88.4km0u5ayaggllYrhzVswzGWcN7Hs1Yi',NULL,'2021-07-07 20:26:14','2021-07-07 20:26:14',46),
(56,'mahasiswa','Jefri Nichol','1931733074','jefri@gmail.com',NULL,'$2y$10$H.Je8IcZZrfkA7csH9GF8O21MntwwByO5LKnbI.jQmOi/WsJC7UUu',NULL,'2021-07-07 20:27:20','2021-07-07 20:27:20',47),
(57,'mahasiswa','Angga Yunanda','1931733077','angga@gmail.com',NULL,'$2y$10$lSYJUFAQAlp7F1ENkiUMYem02krjoaN4CN3JM0T1GbHVSySUmkJue',NULL,'2021-07-07 20:29:58','2021-07-07 20:29:58',48),
(58,'mahasiswa','Renata Putri','1931733078','putri@gmail.com',NULL,'$2y$10$TDqeJlLCAGxPlYhaTektGO0KKec4nvXjdwotGAJBKb6s7E.xEg5Qu',NULL,'2021-07-07 20:31:18','2021-07-07 20:31:18',49),
(59,'mahasiswa','Sukma Fauziah','1931733079','sukma@gmail.com',NULL,'$2y$10$hnRH7pKwOcizmD2xkfSgB.7Z3UhuFe6nhD2B.BPAfHjGgoux2BGfK',NULL,'2021-07-07 20:32:23','2021-07-07 20:32:23',50),
(60,'mahasiswa','Boy William','1931733079','boy@gmail.com',NULL,'$2y$10$fw.P5ic65t09uaq4wNE.CO3n.KiW7MyixM1LZ.McqAZ3sbHkw7HPy',NULL,'2021-07-07 20:33:39','2021-07-07 20:33:39',51),
(61,'mahasiswa','Devi Maharani','1931733080','devi@gmail.com',NULL,'$2y$10$Yhi3cb8394ILJymltB/eme3h4Dk3kubTBWSRMZ8etR9sZn5zIz1qe',NULL,'2021-07-07 20:45:22','2021-07-07 20:45:22',52),
(62,'mahasiswa','Aqilla Adjrah','1931733082','qiw@gmail.com',NULL,'$2y$10$wTnanFxYM8ggQjApOCnml.aF0sPL3Ar6KJPv2GUxXAqxjsFI56Dl2',NULL,'2021-07-07 20:46:57','2021-07-07 20:46:57',53),
(63,'mahasiswa','Rina Diana','1931733082','rina@gmail.com',NULL,'$2y$10$6KCrEh3.N2ww.gxe3HZU.uu0DmD59b39FgjEVDeMHI3g0zFOVBpfG',NULL,'2021-07-07 20:48:00','2021-07-07 20:48:00',54),
(64,'mahasiswa','Riko Lesmana','1931733084','riko@gmail.com',NULL,'$2y$10$6lsOGf0VKO62GXC3C4WGl.05G9J974ko9BTsgvJoygXzud1uW24e6',NULL,'2021-07-07 20:49:27','2021-07-07 20:49:27',55),
(65,'mahasiswa','Shofi Romal Izzul','1931733085','ijul@gmail.com',NULL,'$2y$10$oELsVDGa7EPYi.ssDqv2guEpTdmm/6wwiK3LacJXhS689oxeVrAcC',NULL,'2021-07-07 20:50:38','2021-07-07 20:50:38',56),
(66,'mahasiswa','Afrina Suria','1931733086','nana@gmail.com',NULL,'$2y$10$U/FEcEbmEXJxdQAWlHXhBemYlAKsuuKChnxe9n5UVnkbC2b4KiRHy',NULL,'2021-07-07 20:51:30','2021-07-07 20:51:30',57),
(67,'mahasiswa','Michelle Joana','1931733087','mice@gmail.com',NULL,'$2y$10$p5lyHY3llM12NjmNzxlic.izVD5.GOnXC3i8nCVtjRkYDTc6yPhNe',NULL,'2021-07-07 20:56:51','2021-07-07 20:56:51',58),
(68,'mahasiswa','Yanuar Dicky','1931733087','yanuar@gmail.com',NULL,'$2y$10$70h1E7rep9zn//TgKxQoWu0f/tT/xnQSWWKgWrZo1dmGnVpCZiBIG',NULL,'2021-07-07 20:57:47','2021-07-07 20:57:47',59),
(69,'mahasiswa','Satria Yuda','1931733089','yuda@gmail.com',NULL,'$2y$10$hvvZBJjF3cyNJlNKFhfoZ.AxaAAwKxCO9ajZ.07xyqE5As46t8Nji',NULL,'2021-07-07 20:59:03','2021-07-07 20:59:03',60),
(70,'mahasiswa','Aulya Poppy','1931733090','aul@gmail.com',NULL,'$2y$10$x2SBpiuTIZ66CAIMv2.tdu4vi.dXqx/W508hMWeT1/V1gCGt82COW',NULL,'2021-07-07 21:00:46','2021-07-07 21:00:46',61),
(71,'mahasiswa','Yahya Fatchurrahman','1931733091','yahya@gmail.com',NULL,'$2y$10$ZZg9N07MJjBYP2rW4yCCdOzfnxetnuDZ01mwAEHg3wePTkitxofue',NULL,'2021-07-07 21:01:46','2021-07-07 21:01:46',62),
(72,'mahasiswa','Husein Ahmad','1931733094','husein@gmail.com',NULL,'$2y$10$UEIx25vcvSPZx/3.61Nj2eODVX15OH10ibPtr06aWaBI8ymY2NhaW',NULL,'2021-07-07 21:03:03','2021-07-07 21:03:03',63),
(73,'mahasiswa','Jelita Aprilia','1931733095','lita@gmail.com',NULL,'$2y$10$lTZBcEUeK7vPc5zYK.5BQ.ZMVbjaABUO5mfSZXCXB7tVxskFWrvcC',NULL,'2021-07-07 21:04:08','2021-07-07 21:04:08',64),
(74,'mahasiswa','Momo Aulya','1931733096','momo@gmail.com',NULL,'$2y$10$lLRPqZBz49KjV27fbWsxau2NdaIOCFJp8Vue22bgRDDpqS9Uel37a',NULL,'2021-07-07 21:05:36','2021-07-07 21:05:36',65),
(75,'mahasiswa','Yahya Purnama','1931733097','yahya@gmail.com',NULL,'$2y$10$NlgNSzo8P2kiZKCf04DXI.Bsj2g.857ODzDI2xaFZ2TCCMUQsI//q',NULL,'2021-07-07 21:06:44','2021-07-07 21:06:44',66),
(76,'mahasiswa','Endang Sulastri N','1931733098','endang@gmail.com',NULL,'$2y$10$qFnswo91ws36JWfGo8InOeY7mvVhmlA/O3U2DKq6ML9ZS2o1KhWqm',NULL,'2021-07-07 21:07:42','2021-07-07 21:07:42',67),
(77,'mahasiswa','Ina Marinka','1931733046','ina@gmail.com',NULL,'$2y$10$25AAf.Fru.0TmC2NCMCXiO2PRHardKsgZ2fG.UhSvToLdVDhcqBj2',NULL,'2021-07-07 21:10:25','2021-07-07 21:10:25',68),
(78,'mahasiswa','Stella Okta','1931733047','okta@gmail.com',NULL,'$2y$10$x/I5Xgr387R/uRbzD/5ID.9MEsIx2FIHSNJgcZOr/AYPvvuD5oZXm',NULL,'2021-07-07 21:11:50','2021-07-07 21:11:50',69),
(79,'mahasiswa','Belly Aprillia','1931733049','belly@gmail.com',NULL,'$2y$10$/o6mWZRGcxJWBWFrea8jp.mQUQFFhTjQSflAmfU48FiW8cw4mOOLO',NULL,'2021-07-07 21:12:50','2021-07-07 21:12:50',70),
(80,'mahasiswa','Brandon Salim','1931733049','brandon@gmail.com',NULL,'$2y$10$/b1sDXBUvXvNo1/LDKPzQ.XBh3GmrZ3C8jkKybxAU.GYYQ3.QIWx6',NULL,'2021-07-07 21:14:06','2021-07-07 21:14:06',71),
(81,'mahasiswa','Cristya Putri','1931733050','ptr@gmail.com',NULL,'$2y$10$w4AhsLUMVy7/Apix50tKkO7Uf6iZe4DbR/c6EjhSutcz096li6TMy',NULL,'2021-07-07 21:15:20','2021-07-07 21:15:20',72),
(82,'mahasiswa','Cornelia Putri','1931733051','lia@gmail.com',NULL,'$2y$10$VCRdfmejmKGIRBemGCfcI.o1xp.nWTyJg7TXC1sbfcSnSOsy0RmBK',NULL,'2021-07-07 21:16:26','2021-07-07 21:16:26',73),
(83,'mahasiswa','Devalia Puteri','1931733052','dev@gmail.com',NULL,'$2y$10$EhTdE99ShZoiktXgXI8ZOObGGlxRFpyg.XxfKfEc9/LrbTdZt.Y1a',NULL,'2021-07-07 22:55:40','2021-07-07 22:55:40',74),
(84,'mahasiswa','Devano Putra','1931733053','vano@gmail.com',NULL,'$2y$10$bKyIuQGzvjSkAUneCaQUJ.rI7vR13ioy9Nez4BKF361QSE3K73B4G',NULL,'2021-07-07 22:56:34','2021-07-07 22:56:34',75),
(85,'mahasiswa','Elvin Wicaksana','1931733104','elvin@gmail.com',NULL,'$2y$10$PfR3w8fWFL1zb1uoFJbAWOC6MqRI73lbV0ZU/Gqo0KvcxpeW2MNji',NULL,'2021-07-07 23:03:23','2021-07-07 23:03:23',76),
(86,'mahasiswa','Evi Sinta','1931733105','evi@gmail.com',NULL,'$2y$10$LmQKqHHW2t7LZ3ZKrAde6O9x3xXnin/CtZ3TkaNzWeOUoEGaA5FTi',NULL,'2021-07-07 23:04:25','2021-07-07 23:04:25',77);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
