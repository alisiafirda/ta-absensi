@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <div class="card-body">
            <div class="row mb-2">
                <div class="col">
                    <h5 class="card-title">Data Izin</h5>
                </div>
            </div>
            <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Mata Kuliah</th>
                            <th>Tanggal</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($izin as $i)
                            <tr>                                
                                <td>{{ $i->nim }}</td>
                                <td>{{ $i->nama_lengkap }}</td>
                                <td>MI {{ $i->nama_kelas }}{{ $i->tingkat }} / {{ $i->smt }}</td>
                                <td>{{ $i->nama_matkul }}</td>
                                <td>{{ date('d-m-Y', strtotime($i->tanggal)) }}</td>
                                <td>
                                    <a href="{{ route('cek-izin.edit', $i->no) }}" class="btn btn-warning"><i class="fas fa-info"> </i> </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
