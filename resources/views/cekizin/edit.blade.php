@extends('layout.Admin')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Form Persetujuan Izin</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th colspan="2" class="text-center">MAHASISWA</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>NIM</td>
                                    <td>{{ $i->nim }}</td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{ $i->nama_lengkap }}</td>
                                </tr>
                                <tr>
                                    <td>Kelas</td>
                                    <td>MI {{ $i->nama_kelas }}{{ $i->tingkat }} / {{ $i->smt }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th colspan="2" class="text-center">JADWAL</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Dosen</td>
                                    <td>{{ $i->nama_dosen }}</td>
                                </tr>
                                <tr>
                                    <td>Mata Kuliah</td>
                                    <td>{{ $i->nama_matkul }}</td>
                                </tr>
                                <tr>
                                    <td>Hari</td>
                                    <td>{{ $i->hari }}</td>
                                </tr>
                                <tr>
                                    <td>Jam Mulai</td>
                                    <td>{{ $i->mulai }}</td>
                                </tr>
                                <tr>
                                    <td>Jam Selesai</td>
                                    <td>{{ $i->selesai }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <form action="{{ route('cek-izin.update', $i->no) }}" method="post">
                            @csrf
                            @method('patch')
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <th colspan="2" class="text-center">IZIN</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>{{ date('d-m-Y', strtotime($i->tanggal)) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Ket.</td>
                                        <td>{{ $i->keterangan }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="p-0">
                                            <img src='{{ asset("bukti/$i->bukti") }}' alt="" style="width: 100%; height: 285px; object-fit: cover;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Approval</td>
                                        <td class="p-2">
                                            <select name="approval" class="form-control form-control-sm" required>
                                                <option value="">-- pilih satu --</option>
                                                <option value="1">Setujui</option>
                                                <option value="0">Tidak Setujui</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="p-2" style="text-align: right;">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
