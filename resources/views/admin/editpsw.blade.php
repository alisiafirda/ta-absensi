@extends('layout.Admin')
@section('content')

    <div class="col-md-12">
        <div class="card">

            <div class="card-body">
                <h4 class="card-title">Edit Password Admin</h4>
                <div class="tab-pane" id="password">
                    <form class="form-horizontal" action="/update-password-admin/{{ $admin->id }}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="inputName" class="col-sm-3 text-end control-label col-form-label">Password Lama</label>
                            <div class="col-sm-9">
                                <input type="password" name="old_password" class="form-control" placeholder="Password Lama" value="{{ old('old_password') }}">
                                @if (session()->get('old_password'))
                                    {{ session()->get('old_password') }}
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-3 text-end control-label col-form-label">Password Baru</label>
                            <div class="col-sm-9">
                                <input type="password" name="password" class="form-control" placeholder="Password Baru" value="{{ old('password') }}">
                                @error('password')
                                    <p class="text-danger">
                                        {{ $message }}
                                    </p>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail" class="col-sm-3 text-end control-label col-form-label">Konfirmasi Password Baru</label>
                            <div class="col-sm-9">
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Konfirmasi Password Baru" value="{{ old('password_confirmation') }}">
                                @error('password_confirmation')
                                    <p class="text-danger">
                                        {{ $message }}
                                    </p>
                                @enderror
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <a href="/edit-admin/{{ $admin->id }}" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                                <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>


        </div>
    </div>
@endsection
