@extends('layout.Admin')
@section('content')               
                
                <div class="col-md-12">
                        <div class="card">
                        <form action="/update-semester/{{$semester->id_semester}}" method="post">
                            @csrf
                                <div class="card-body">
                                    <h4 class="card-title">Edit Semester</h4>
                                    <div class="form-group row">
                                        <label for="fname"
                                            class="col-sm-3 text-end control-label col-form-label">Semester</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="smt" class="form-control" autofocus required id="fname"
                                                placeholder="Masukkan Semester" value="{{$semester->semester}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <a href="/semester" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                                    </div>
                                </div>
                            </form>
                        </div>
</div>
@endsection