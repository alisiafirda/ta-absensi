@extends('layout.Admin')
@section('content')               
                
                <div class="col-md-12">
                        <div class="card">
                        <form action="/add_dosen" method="post">
                            @csrf
                                <div class="card-body">
                                    <h4 class="card-title">Tambah Dosen</h4>
                                    <div class="form-group row">
                                        <label for="fname"
                                            class="col-sm-3 text-end control-label col-form-label">NIDN</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="nidn" id="nidn" class="form-control" autofocus required id="fname"
                                                placeholder="Masukkan NIDN" maxlength=10>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-end control-label col-form-label">Nama Dosen</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="namadosen" class="form-control" autofocus required id="lname"
                                                placeholder="Masukkan Nama Dosen">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="jenis_kelamin" class="col-sm-3 text-end control-label col-form-label">Jenis Kelamin</label>
                                        <div class="col-sm-9">
                                            <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off" >
                                                <option value="" selected>-- Pilih Jenis Kelamin --</option>
                                                <option value="Laki-Laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-end control-label col-form-label">No. HP</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="hp" class="form-control" autofocus required id="lname"
                                                placeholder="Masukkan No. HP">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="matkul" class="col-sm-3 text-end control-label col-form-label">Mata Kuliah</label>
                                        <div class="col-sm-9">
                                            <select name="matkul" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off" >
                                                <option value="" selected>-- Pilih Mata Kuliah --</option>
                                                @foreach ($matkul as $item)
                                                <option value="{{$item->kode_matkul}}">{{$item->nama_matkul}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="semester" class="col-sm-3 text-end control-label col-form-label">Semester</label>
                                        <div class="col-sm-9">
                                            <select name="semester" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off" >
                                                <option value="" selected>-- Pilih Semester --</option>
                                                @foreach ($semester as $item)
                                                <option value="{{$item->id_semester}}">{{$item->semester}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="tingkat" class="col-sm-3 text-end control-label col-form-label">Tingkat</label>
                                        <div class="col-sm-9">
                                            <select name="tingkat" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off" >
                                                <option value="" selected>-- Pilih Tingkat --</option>
                                                @foreach ($tingkat as $item)
                                                <option value="{{$item->id_tingkat}}">{{$item->tingkat}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="kelas" class="col-sm-3 text-end control-label col-form-label">Kelas</label>
                                        <div class="col-sm-9">
                                            <select name="kelas" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off" >
                                                <option value="" selected>-- Pilih Kelas --</option>
                                                @foreach ($kelas as $item)
                                                <option value="{{$item->id_kelas}}">{{$item->nama_kelas}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                    <a href="/dosen" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                                    </div>
                                </div>
                            </form>
                        </div>
</div>
@endsection