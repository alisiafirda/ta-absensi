@extends('layout.Admin')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <form action="/update-dosen/{{ $dosen->nidn }}" method="post">
                @csrf
                <div class="card-body">
                    <h4 class="card-title">Edit Dosen</h4>
                    <div class="form-group row">
                        <label class="col-sm-3 text-end control-label col-form-label">NIDN</label>
                        <div class="col-sm-9">
                            <input type="text" name="nidn" class="form-control" autofocus required placeholder="Masukkan NIDN" value="{{ $dosen->nidn }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-end control-label col-form-label">Nama Dosen</label>
                        <div class="col-sm-9">
                            <input type="text" name="namadosen" class="form-control" value="{{ $dosen->nama_dosen }}" autofocus required placeholder="Masukkan Nama Dosen">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_kelamin" class="col-sm-3 text-end control-label col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="Laki-Laki" @if ($dosen->jenis_kelamin == 'Laki-Laki') selected @endif>Laki-Laki</option>
                                <option value="Perempuan" @if ($dosen->jenis_kelamin == 'Perempuan') selected @endif>Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 text-end control-label col-form-label">No. HP</label>
                        <div class="col-sm-9">
                            <input type="number" name="hp" class="form-control" value="{{ $dosen->hp }}" autofocus required placeholder="Masukkan No. HP">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="matkul" class="col-sm-3 text-end control-label col-form-label">Mata Kuliah</label>
                        <div class="col-sm-9">
                            <select name="matkul" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Mata Kuliah --</option>
                                @foreach ($matkul as $item)
                                    <option {{ $item->kode_matkul == $dosen->nama_matkul ? 'selected' : '' }} value="{{ $item->kode_matkul }}">{{ $item->nama_matkul }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label for="semester" class="col-sm-3 text-end control-label col-form-label">Semester</label>
                        <div class="col-sm-9">
                            <select name="semester" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Semester --</option>
                                @foreach ($semester as $item)
                                    <option {{ $item->id_semester == $dosen->semester ? 'selected' : '' }} value="{{ $item->id_semester }}">{{ $item->semester }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tingkat" class="col-sm-3 text-end control-label col-form-label">Tingkat</label>
                        <div class="col-sm-9">
                            <select name="tingkat" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Tingkat --</option>
                                @foreach ($tingkat as $item)
                                    <option {{ $item->id_tingkat == $dosen->tingkat ? 'selected' : '' }} value="{{ $item->id_tingkat }}">{{ $item->tingkat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label for="kelas" class="col-sm-3 text-end control-label col-form-label">Kelas</label>
                        <div class="col-sm-9">
                            <select name="id_kelas" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Kelas --</option>
                                @foreach ($kls as $i)
                                    <option value="{{ $i->id_kelas }}" {{ $dosen->nama_kelas == $i->id_kelas ? 'selected' : '' }}>MI {{ $i->nama_kelas }}{{ $i->tingkat }} / {{ $i->smt }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    {{-- <div class="form-group row">
                                        <label class="col-sm-3 text-end control-label col-form-label">Mata Kuliah</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="matkul" class="form-control" 
                                            value="{{$dosen->nama_matkul}}" autofocus required 
                                                placeholder="Masukkan Mata Kuliah" readonly> 
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-end control-label col-form-label">Semester</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="smt" class="form-control" 
                                            value="{{$dosen->semester}}" autofocus required 
                                                placeholder="Masukkan Semester"> 
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 text-end control-label col-form-label">Kelas</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="kls" class="form-control" 
                                            value="{{$dosen->nama_kelas}}" autofocus required 
                                                placeholder="Masukkan Kelas"> 
                                        </div>
                                    </div> --}}
                    <!-- <div class="form-group row">
                                            <label for="cono1"
                                                class="col-sm-3 text-end control-label col-form-label">Agama</label>
                                            <div class="col-sm-9">
                                            <select name="agama" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off" >
                                            <option value="">--Pilih Agama--</option>
                                            <option value="Islam" @if ($dosen->agama == 'Islam') selected @endif >Islam</option>
                                            <option value="Kristen" @if ($dosen->agama == 'Kristen') selected @endif >Kristen</option>
                                            <option value="Budda" @if ($dosen->agama == 'Budda') selected @endif >Budda</option>
                                            <option value="Hindu" @if ($dosen->agama == 'Hindu') selected @endif >Hindu</option>
                                            <option value="Katolik" @if ($dosen->agama == 'Katolik') selected @endif >Katolik</option>
                                            <option value="Konghucu" @if ($dosen->agama == 'Konghucu') selected @endif >Konghucu</option>
                                            </select>
                                            </div>
                                        </div> -->
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <a href="/dosen" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
