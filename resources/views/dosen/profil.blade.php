@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Profil</h3>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tr>
                    <td>NIDN</td>
                    <td>{{ $data->nidn }}</td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>{{ $data->nama_dosen }}</td>
                </tr>
                <tr>
                    <td>No Telepon</td>
                    <td>{{ $data->hp }}</td>
                </tr>
                <tr>
                    <td>Mata Kuliah</td>
                    <td>{{ $data->nama_matkul }}</td>
                </tr>
            </table>
        </div>
    </div>
@endsection
