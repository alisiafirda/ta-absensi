@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <div class="card-body">
            <div class="row mb-2">
                <div class="col">
                    <h5 class="card-title">Data Dosen</h5>
                </div>
                <div class="col-1">
                    <a href="/dosen/form-tambah" class="btn btn-success float-right"><i class="fas fa-plus"> </i> </a>
                </div>
            </div>
            <!-- <h5 class="card-title">Data Dosen</h5><a href="/dosen/form-tambah" class="btn btn-success"><i class="fas fa-plus"> </i> </a> -->
            <form action="">
                <div class="row form-group">
                    <div class="col-lg-5">
                        <select name="id_kelas" id="id_kelas" class="form-control" required>
                            <option value="">-- pilih kelas --</option>
                            @foreach ($kls as $i)
                                <option value="{{ $i->id_kelas }}">MI {{ $i->nama_kelas }}{{ $i->tingkat }} / {{ $i->smt }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <button type="submit" class="btn btn-success">FILTER&ensp;<i class="fas fa-filter"></i></button>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIDN</th>
                            <th>Nama Dosen</th>
                            <th>Jenis Kelamin</th>
                            <th>No. HP</th>
                            <th>Mata Kuliah</th>
                            <th>Semester</th>
                            <th>Tingkat</th>
                            <th>Kelas</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dosen as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->nidn }}</td>
                                <td>{{ $item->nama_dosen }}</td>
                                <td>{{ $item->jenis_kelamin }}</td>
                                <td>{{ $item->hp }}</td>
                                <td class="text-center">
                                    @php
                                        $jam = date('H:i:s');
                                        $jadwal = \App\Models\Jadwal::select('jadwal.*')
                                            ->join('jwd_jm AS jw', 'jw.id_jadwal', 'jadwal.id_jadwal')
                                            ->join('jam AS j', function ($j) use ($jam) {
                                                $j->on('jw.id_jam', 'j.id_jam')
                                                    ->where('j.mulai', '<=', $jam)
                                                    ->where('j.selesai', '>=', $jam);
                                            })
                                            ->where('jadwal.nidn', '=', $item->nidn)
                                            ->whereRaw('DAYOFWEEK(CURDATE()) = int_hari')
                                            ->first();
                                        // if (isset($jadwal->jam_mulai) && isset($jadwal->jam_selesai)) {
                                        //     $start = $jadwal->jam_mulai;
                                        //     $end = $jadwal->jam_selesai;
                                        // }
                                        if (isset($jadwal->kode_matkul)) {
                                            $matkul = \App\Models\Matkul::select('matkul.*')
                                                ->where('matkul.kode_matkul', '=', $jadwal->kode_matkul)
                                                ->first();
                                        } else {
                                            $matkul = null;
                                        }
                                    @endphp
                                    @if (isset($matkul->nama_matkul))
                                        <span class="badge bg-success fs-6 fw-bold">
                                            {{ $matkul->nama_matkul }}
                                        </span>
                                    @else
                                        <span class="badge bg-danger fs-6 fw-bold">
                                            Kosong
                                        </span>
                                    @endif
                                </td>
                                <td>{{ $item->semester }}</td>
                                <td>{{ $item->tingkat }}</td>
                                <td>{{ $item->nama_kelas }}</td>
                                <td>
                                    <a href="/edit-dosen/{{ $item->id }}" class="btn btn-warning"><i class="fas fa-edit"> </i> </a>
                                    <a href="/hapus-dosen/{{ $item->id }}" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Menghapus Data Ini?')"><i class="fas fa-trash-alt"> </i> </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
