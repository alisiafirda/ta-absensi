@extends('layout.Mahasiswa')
@section('extracss')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap5/css/bootstrap.min.css') }}">
    <link href="{{ asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <style>
        .page-link {
            color: #fff;
            pointer-events: none;
            background-color: #575757;
            border-color: #575747;
        }

    </style>
@endsection

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="container mx-5">
        <header class="major">
            <h3>JADWAL KELAS MAHASISWA MANAJEMEN INFORMATIKA</h3>
        </header>
        <div class="table-wrapper row4">
            <table class="alt datatable">
                <thead>
                    <tr>
                        <th>Kode Matkul</th>
                        <th>Hari</th>
                        <th>Jam Mulai</th>
                        <th>Jam Selesai</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($jadwal as $item)
                        <tr>
                            <td>{{ $item->matkul->nama_matkul }}</td>
                            <td>{{ $item->hari }}</td>
                            <td>{{ $item->mulai }}</td>
                            <td>{{ $item->selesai }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- $jadwal->links() --}}
        </div>
    </div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/assets/extra-libs/multicheck/datatable-checkbox-init.js') }}"></script>
    <script src="{{ asset('admin/assets/extra-libs/multicheck/jquery.multicheck.js') }}"></script>
    <script src="{{ asset('admin/assets/extra-libs/DataTables/datatables.min.js') }}"></script>

    <script>
        $(function() {
            $('.datatable').DataTable({
                dom: '<"table-wrapper row4"t<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7" p>>'
            });
        })

    </script>
@endsection
