@extends('layout.Admin')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <form action="/add_mahasiswa" method="post">
                @csrf
                <div class="card-body">
                    <h4 class="card-title">Tambah Mahasiswa</h4>
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-end control-label col-form-label">NIM</label>
                        <div class="col-sm-9">
                            <input type="text" name="nim" class="form-control" autofocus required id="fname" placeholder="Masukkan NIM">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lname" class="col-sm-3 text-end control-label col-form-label">Nama Mahasiswa</label>
                        <div class="col-sm-9">
                            <input type="text" name="namamhs" class="form-control" autofocus required id="lname" placeholder="Masukkan Nama Mahasiswa">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lname" class="col-sm-3 text-end control-label col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" class="form-control" autofocus required id="lname" placeholder="Masukkan Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="id_kelas" class="col-sm-3 text-end control-label col-form-label">Kelas</label>
                        <div class="col-sm-9">
                            <select name="id_kelas" id="id_kelas" class="form-control" required>
                                <option value="">-- pilih kelas --</option>
                                @foreach ($kls as $i)
                                    <option value="{{ $i->id_kelas }}">MI {{ $i->nama_kelas }}{{ $i->tingkat }} / {{ $i->smt }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <label for="id_tingkat" class="col-sm-3 text-end control-label col-form-label">Tingkat</label>
                        <div class="col-sm-9">
                            <select name="id_tingkat" id="id_tingkat" class="form-control" required>
                                <option value="">-- pilih tingkat --</option>
                                @foreach ($tingkat as $i)
                                    <option value="{{ $i->id_tingkat }}">{{ $i->tingkat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="id_semester" class="col-sm-3 text-end control-label col-form-label">Semester</label>
                        <div class="col-sm-9">
                            <select name="id_semester" id="id_semester" class="form-control" required>
                                <option value="">-- pilih semester --</option>
                                @foreach ($smt as $i)
                                    <option value="{{ $i->id_semester }}">{{ $i->semester }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label for="email1" class="col-sm-3 text-end control-label col-form-label">Tempat, Tanggal Lahir</label>
                        <div class="col-sm-9">
                            <input type="text" name="ttl" class="form-control" autofocus required id="email1" placeholder="Masukkan Tempat, Tanggal Lahir">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_kelamin" class="col-sm-3 text-end control-label col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <select name="jenis_kelamin" class="form-control" id="jenis_kelamin" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Jenis Kelamin --</option>
                                <option value="Laki-Laki">Laki-Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cono1" class="col-sm-3 text-end control-label col-form-label">Alamat</label>
                        <div class="col-sm-9">
                            <textarea name="alamat" class="form-control" autofocus required placeholder="Masukkan Alamat Lengkap"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="agama" class="col-sm-3 text-end control-label col-form-label">Agama</label>
                        <div class="col-sm-9">
                            <select name="agama" class="form-control" id="agama" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Agama --</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Buddha">Buddha</option>
                                <option value="Khonghucu">Khonghucu</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <a href="/mahasiswa" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
