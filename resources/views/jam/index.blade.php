@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="card-body">
            <div class="row mb-2">
                <div class="col">
                    <h5 class="card-title">Data Jam</h5>
                </div>
                <div class="col-1">
                    <a href="{{ route('jam.create') }}" class="btn btn-success float-right"><i class="fas fa-plus"> </i> </a>
                </div>
            </div>
            <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Jam</th>
                            <th>Mulai</th>
                            <th>Selesai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jam as $i)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $i->jam }}</td>
                                <td>{{ $i->mulai }}</td>
                                <td>{{ $i->selesai }}</td>
                                <td>
                                    <form action="{{ route('jam.destroy', $i->id_jam) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <a href="{{ route('jam.edit', $i->id_jam) }}" class="btn btn-warning"><i class="fas fa-edit"> </i> </a>
                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Menghapus Data Ini?')"><i class="fas fa-trash-alt"> </i> </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
