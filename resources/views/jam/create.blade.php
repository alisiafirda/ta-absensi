@extends('layout.Admin')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Jam</h3>
            </div>
            <form action="{{ route('jam.store') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <label for="jam" class="col-sm-3 text-end control-label col-form-label">Jam</label>
                        <div class="col-sm-9">
                            <input type="text" name="jam" class="form-control" autofocus required id="jam" placeholder="Jam -">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jm" class="col-sm-3 text-end control-label col-form-label">Jam Mulai</label>
                        <div class="col-sm-9">
                            <input type="time" name="jm" class="form-control" autofocus required id="jm" placeholder="Masukkan Hari" value="{{ old('jm') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="js" class="col-sm-3 text-end control-label col-form-label">Jam Selesai</label>
                        <div class="col-sm-9">
                            <input type="time" name="js" class="form-control" autofocus required id="js" placeholder="Masukkan Hari" value="{{ old('js') }}">
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <a href="{{ route('jam.index') }}" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
