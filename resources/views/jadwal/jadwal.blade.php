@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <div class="card-body">
            <div class="row mb-2">
                <div class="col">
                    <h5 class="card-title">Data Jadwal</h5>
                </div>
                <div class="col-1">
                    <a href="/jadwal/tambah" class="btn btn-success float-right"><i class="fas fa-plus"> </i> </a>
                </div>
            </div>
            <form action="">
                <div class="row form-group">
                    <div class="col-lg-5">
                        <select name="id_kelas" id="id_kelas" class="form-control" required>
                            <option value="">-- pilih kelas --</option>
                            @foreach ($kls as $i)
                                <option value="{{ $i->id_kelas }}">MI {{ $i->nama_kelas }}{{ $i->tingkat }} / {{ $i->smt }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <button type="submit" class="btn btn-success">FILTER&ensp;<i class="fas fa-filter"></i></button>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Semester</th>
                            <th>Tingkat</th>
                            <th>Kelas</th>
                            <th>Mata Kuliah</th>
                            <th>Hari</th>
                            <th>Hari ke</th>
                            <th>Nama Dosen</th>
                            <th>Jam Mulai</th>
                            <th>Jam Selesai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jadwal as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->semester->semester }}</td>
                                <td>{{ $item->tingkat['tingkat'] }}</td>
                                <td>{{ $item->kelas->nama_kelas }}</td>
                                <td>{{ $item->matkul->nama_matkul }}</td>
                                <td>{{ $item->hari }}</td>
                                <td>{{ $item->int_hari }}</td>
                                <td>{{ $item->nama_dosen }}</td>
                                <td>{{ $item->mulai }}</td>
                                <td>{{ $item->selesai }}</td>
                                <td>
                                    <a href="/edit-jadwal/{{ $item->id_jadwal }}" class="btn btn-warning"><i class="fas fa-edit"> </i> </a>
                                    <a href="/hapus-jadwal/{{ $item->id_jadwal }}" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Menghapus Data Ini?')"><i class="fas fa-trash-alt"> </i> </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
