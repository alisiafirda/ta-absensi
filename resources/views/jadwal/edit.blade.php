@extends('layout.Admin')
@section('extracss')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/libs/select2/dist/css/select2.min.css') }}">
@endsection
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="col-md-12">
        <div class="card">
            <form action="/update-jadwal/{{ $jadwal->id_jadwal }}" method="post">
                @csrf
                <div class="card-body">
                    <h4 class="card-title">Edit Jadwal</h4>
                    {{-- <div class="form-group row">
                        <label for="semester" class="col-sm-3 text-end control-label col-form-label">Semester</label>
                        <div class="col-sm-9">
                            <select name="semester" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Semester --</option>
                                @foreach ($semester as $item)
                                    <option {{ $item->id_semester == $jadwal->id_semester ? 'selected' : '' }} value="{{ $item->id_semester }}">{{ $item->semester }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}
                    {{-- <div class="form-group row">
                        <label for="tingkat" class="col-sm-3 text-end control-label col-form-label">Tingkat</label>
                        <div class="col-sm-9">
                            <select name="tingkat" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Tingkat --</option>
                                @foreach ($tingkat as $item)
                                    <option {{ $item->id_tingkat == $jadwal->id_tingkat ? 'selected' : '' }} value="{{ $item->id_tingkat }}">{{ $item->tingkat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}
                    {{-- <div class="form-group row">
                        <label for="kelas" class="col-sm-3 text-end control-label col-form-label">Kelas</label>
                        <div class="col-sm-9">
                            <select name="id_kelas" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Kelas --</option>
                                @foreach ($kelas as $item)
                                    <option {{ $item->id_kelas == $jadwal->id_kelas ? 'selected' : '' }} value="{{ $item->id_kelas }}">{{ $item->nama_kelas }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label for="id_kelas" class="col-sm-3 text-end control-label col-form-label">Kelas</label>
                        <div class="col-sm-9">
                            <select name="id_kelas" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="">-- Pilih Kelas --</option>
                                @foreach ($kls as $i)
                                    <option value="{{ $i->id_kelas }}" {{ $jadwal->id_kelas == $i->id_kelas ? 'selected' : '' }}>MI {{ $i->nama_kelas }}{{ $i->tingkat }} / {{ $i->smt }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="matkul" class="col-sm-3 text-end control-label col-form-label">Mata Kuliah</label>
                        <div class="col-sm-9">
                            <select name="matkul" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Mata Kuliah --</option>
                                @foreach ($matkul as $item)
                                    <option {{ $item->kode_matkul == $jadwal->kode_matkul ? 'selected' : '' }} value="{{ $item->kode_matkul }}">{{ $item->nama_matkul }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cono1" class="col-sm-3 text-end control-label col-form-label">Hari</label>
                        <div class="col-sm-9">
                            <input type="text" name="hari" class="form-control" autofocus required id="cono1" placeholder="Masukkan Hari" value="{{ $jadwal->hari }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dosen" class="col-sm-3 text-end control-label col-form-label">Nama Dosen</label>
                        <div class="col-sm-9">
                            <select name="nidn" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Nama Dosen --</option>
                                @foreach ($dosen as $item)
                                    <option {{ $item->nidn == $jadwal->nidn ? 'selected' : '' }} value="{{ $item->nidn }}">{{ $item->nama_dosen }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dosen" class="col-sm-3 text-end control-label col-form-label">Jam</label>
                        <div class="col-md-9">
                            <select name="id_jam[]" class="select2 form-select shadow-none mt-3" multiple="multiple" style="height: 36px;width: 100%;" required>
                                @foreach ($jam as $i)
                                    <option value="{{ $i->id_jam }}" {{ in_array($i->id_jam, $jwd_jm) ? 'selected' : '' }}>{{ "$i->jam ($i->mulai - $i->selesai)" }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                                        <label for="cono1"
                                            class="col-sm-3 text-end control-label col-form-label">Jam Mulai</label>
                                        <div class="col-sm-9">
                                            <input type="time" name="jm" class="form-control" autofocus required id="cono1"
                                                placeholder="Masukkan Hari" value="{{$jadwal->jam_mulai}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cono1"
                                            class="col-sm-3 text-end control-label col-form-label">Jam Selesai</label>
                                        <div class="col-sm-9">
                                            <input type="time" name="js" class="form-control" autofocus required id="cono1"
                                                placeholder="Masukkan Hari" value="{{$jadwal->jam_selesai}}">
                                        </div>
                                    </div> --}}
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <a href="/jadwal" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/assets/libs/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/assets/libs/select2/dist/js/select2.min.js') }}"></script>

    <script>
        $(function() {
            $(".select2").select2({
                placeholder: "-- pilih jam --",
            });
        });

    </script>
@endsection
