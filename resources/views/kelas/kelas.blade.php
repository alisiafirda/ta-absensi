@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="card">

        <div class="card-body">
            <div class="row mb-2">
                <div class="col">
                    <h5 class="card-title">Data Kelas</h5>
                </div>
                <div class="col-1">
                    <a href="/kelas/tambah" class="btn btn-success float-right"><i class="fas fa-plus"> </i> </a>
                </div>
            </div>
            <form action="">
                <div class="row form-group">
                    <div class="col-lg-3">
                        <select name="id_tingkat" class="form-control">
                            <option value="">-- pilih tingkat --</option>
                            @foreach ($tingkat as $v)
                                <option value="{{ $v->id_tingkat }}" {{ request()->input('id_tingkat') == $v->id_tingkat ? 'selected' : '' }}>{{ $v->tingkat }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <select name="id_semester" class="form-control">
                            <option value="">-- pilih semester --</option>
                            @foreach ($smt as $v)
                                <option value="{{ $v->id_semester }}" {{ request()->input('id_semester') == $v->id_semester ? 'selected' : '' }}>{{ $v->semester }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <button type="submit" class="btn btn-success">FILTER&ensp;<i class="fas fa-filter"></i></button>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Kelas</th>
                            <th>Semester</th>
                            <th>Tingkat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    </tbody>
                    @foreach ($kelas as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->nama_kelas }}</td>
                            <td>{{ $item->smt }}</td>
                            <td>{{ $item->nm_tingkat }}</td>
                            <td>
                                <a href="{{ route('admin.kelas.info', ['id' => $item->id_kelas]) }}" class="btn btn-info"><i class="fas fa-info"></i></a>
                                <a href="/edit-kelas/{{ $item->id_kelas }}" class="btn btn-warning"><i class="fas fa-edit"> </i> </a>
                                <a href="/hapus-kelas/{{ $item->id_kelas }}" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Menghapus Data Ini?')"><i class="fas fa-trash-alt"> </i> </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
