@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Info Kelas</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="mb-3">
                        <a href="{{ route('admin.kelas.rekapAbsen', ['id' => $kls->id_kelas]) }}" class="btn btn-success d-block text-uppercase font-weight-bold" style="font-weight: bold">Export Data Komulatif Absensi&emsp;<i class="fas fa-file-excel"></i></a>
                    </div>
                    <table class="table table-striped">
                        <tr>
                            <td>Nama Kelas</td>
                            <td>{{ $kls->nama_kelas }}</td>
                        </tr>
                        <tr>
                            <td>Tingkat</td>
                            <td>{{ $kls->tingkat }}</td>
                        </tr>
                        <tr>
                            <td>Semester</td>
                            <td>{{ $kls->semester }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-6">
                    <table class="table table-striped table-hover" id="table-mhs">
                        <thead>
                            <th>NIM</th>
                            <th>Nama</th>
                        </thead>
                        <tbody>
                            @foreach ($mhs as $m)
                                <tr>
                                    <td>{{ $m->nim }}</td>
                                    <td>{{ $m->nama_mhs }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script>
        $(function() {
            $('#table-mhs').DataTable();
        });

    </script>
@endsection
