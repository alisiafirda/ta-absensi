@extends('layout.Admin')
@section('content')

    <div class="col-md-12">
        <div class="card">
            <form action="/update-kelas/{{ $kelas->id_kelas }}" method="post">
                @csrf
                <div class="card-body">
                    <h4 class="card-title">Edit Kelas</h4>
                    <div class="form-group row">
                        <label for="fname" class="col-sm-3 text-end control-label col-form-label">Nama Kelas</label>
                        <div class="col-sm-9">
                            <input type="text" name="nmkls" class="form-control" autofocus required id="fname" placeholder="Masukkan Kelas" value="{{ $kelas->nama_kelas }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="semester" class="col-sm-3 text-end control-label col-form-label">Semester</label>
                        <div class="col-sm-9">
                            <select name="smt" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Semester --</option>
                                @foreach ($semester as $item)
                                    <option {{ $item->id_semester == $kelas->semester ? 'selected' : '' }} value="{{ $item->id_semester }}">{{ $item->semester }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tingkat" class="col-sm-3 text-end control-label col-form-label">Tingkat</label>
                        <div class="col-sm-9">
                            <select name="tk" class="form-control" id="exampleFormControlSelect1" placeholder="" autocomplete="off">
                                <option value="" selected>-- Pilih Tingkat --</option>
                                @foreach ($tingkat as $item)
                                    <option {{ $item->id_tingkat == $kelas->tingkat ? 'selected' : '' }} value="{{ $item->id_tingkat }}">{{ $item->tingkat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <a href="/kelas" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
