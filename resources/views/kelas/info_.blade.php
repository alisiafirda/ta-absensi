@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Info Kelas</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Export Data Komulatif Absensi</h5>
                    <div class="row">
                        <div class="col-lg-7">
                            <form action="{{ route('admin.kelas.info', ['id' => $kls->id_kelas]) }}">
                                <div class="input-group mb-3">
                                    <select name="matkul" id="matkul" class="form-control" required>
                                        <option value="">-- pilih mata kuliah --</option>
                                        @foreach ($jwd as $j)
                                            <option value="{{ $j->kode_matkul }}" {{ request()->input('matkul') == $j->kode_matkul ? 'selected' : '' }}>{{ $j->nama_matkul }}</option>
                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-success"><i class="fas fa-filter"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-5">
                            <form action="{{ route('admin.kelas.rekapAbsen') }}">
                                <input type="hidden" name="matkul" value="{{ request()->input('matkul') }}">
                                <div class="input-group mb-3">
                                    <select name="tgl" id="tgl" class="form-control" {{ request()->has('matkul') ? 'required' : 'disabled' }}>
                                        <option value="">-- pilih tanggal --</option>
                                        @isset($absen)
                                            @foreach ($absen as $a)
                                                <option value="{{ $a->tanggal }}">{{ $a->tanggal }}</option>
                                            @endforeach
                                        @endisset
                                    </select>
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-success" {{ request()->has('matkul') ? '' : 'disabled' }}><i class="fas fa-file-excel"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <table class="table table-striped">
                        <tr>
                            <td>Nama Kelas</td>
                            <td>{{ $kls->nama_kelas }}</td>
                        </tr>
                        <tr>
                            <td>Tingkat</td>
                            <td>{{ $kls->tingkat }}</td>
                        </tr>
                        <tr>
                            <td>Semester</td>
                            <td>{{ $kls->semester }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-6">
                    <table class="table table-striped table-hover" id="table-mhs">
                        <thead>
                            <th>NIM</th>
                            <th>Nama</th>
                        </thead>
                        <tbody>
                            @foreach ($mhs as $m)
                                <tr>
                                    <td>{{ $m->nim }}</td>
                                    <td>{{ $m->nama_mhs }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extrajs')
    <script>
        $(function() {
            $('#table-mhs').DataTable();
        });

    </script>
@endsection
