<table>
    <tr>
        <td colspan="2"><b>KELAS</b></td>
        <td>{{ $kls->nama_kelas }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>TINGKAT</b></td>
        <td>{{ $kls->nm_tingkat }}</td>
    </tr>
    <tr>
        <td colspan="2"><b>SEMESTER</b></td>
        <td>{{ $kls->smt }}</td>
    </tr>
</table>
<table>
    <thead>
        <tr>
            <th align="center" valign="middle" rowspan="2">NO</th>
            <th align="center" valign="middle" rowspan="2">NIM</th>
            <th align="center" valign="middle" rowspan="2">NAMA</th>
            <th align="center" colspan="3">KOMULATIF ABSENSI</th>
            <th align="center" valign="middle" rowspan="2">AIS</th>
            <th align="center" valign="middle" rowspan="2">KET</th>
        </tr>
        <tr>
            <th align="center">A</th>
            <th align="center">I</th>
            <th align="center">S</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rekap as $k => $r)
            @php
                if ($r->alpha > 44) {
                    $ket = 'SP 3';
                } elseif ($r->alpha > 36) {
                    $ket = 'SP 2';
                } elseif ($r->alpha > 18) {
                    $ket = 'SP 1';
                } else {
                    $ket = '-';
                }
            @endphp
            <tr>
                <td align="center">{{ $k + 1 }}</td>
                <td align="center">{{ $r->nim }}</td>
                <td>{{ $r->nama_mhs }}</td>
                <td align="center">{{ $r->alpha }}:0</td>
                <td align="center">{{ $r->ijin }}:0</td>
                <td align="center">{{ $r->sakit }}:0</td>
                <td align="center">{{ $r->total }}:0</td>
                <td align="center">{{ $ket }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
