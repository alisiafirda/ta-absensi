<html>
<table>
    <tr>
        <td colspan="2"><b>Nama : </b></td>
        <td><b>{{ $mhs->nama_mhs }}</b></td>
    </tr>
    <tr>
        <td colspan="2"><b>NIM : </b></td>
        <td align="left"><b>{{ $mhs->nim }}</b></td>
    </tr>
    <tr>
        <td colspan="2"><b>Kelas : </b></td>
        <td><b>{{ $mhs->nm_tingkat . $mhs->nama_kelas }}</b></td>
    </tr>
    <tr>
        <td colspan="2"><b>Program Studi : </b></td>
        <td><b>{{ $mhs->prodi }}</b></td>
    </tr>
    <tr>
        <td colspan="2"><b>Tahun Akademik : </b></td>
        <td><b>{{ $mhs->smt }}</b></td>
    </tr>
</table>
<table style="border-color: black">
    <thead>
        <tr>
            <td align="center">NO</td>
            <td align="center">MATA KULIAH</td>
            <td align="center">TANGGAL</td>
            <td align="center">ALPHA</td>
            <td align="center">IJIN</td>
            <td align="center">SAKIT</td>
        </tr>
    </thead>
    <tbody>
        @php
            $alpha = $ijin = $sakit = 0;
        @endphp
        @foreach ($absen as $k => $abs)
            @php
                $alpha = $alpha + $abs->alpha;
                $ijin = $ijin + $abs->ijin;
                $sakit = $sakit + $abs->sakit;
            @endphp
            <tr>
                <td align="center">{{ $k + 1 }}</td>
                <td>{{ $abs->matkul }}</td>
                <td align="center">{{ $abs->tanggal }}</td>
                <td align="center">{{ $abs->alpha }}:00</td>
                <td align="center">{{ $abs->ijin }}:00</td>
                <td align="center">{{ $abs->sakit }}:00</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="3"></td>
            <td align="center">{{ $alpha }}:00</td>
            <td align="center">{{ $ijin }}:00</td>
            <td align="center">{{ $sakit }}:00</td>
        </tr>
        @php
            if ($alpha > 44) {
                    $ket = 'SP 3';
                } elseif ($alpha > 36) {
                    $ket = 'SP 2';
                } elseif ($alpha > 18) {
                    $ket = 'SP 1';
                } else {
                    $ket = '-';
                }
        @endphp
        <tr>
            <td colspan="3" align="right"><b>KETERANGAN :</b></td>
            <td colspan="3"><b>{{ $ket }}</b></td>
        </tr>
    </tbody>
</table>

</html>
