@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Jadwal Hari Ini</h3>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Mata Kuliah</th>
                            <th>Kelas</th>
                            <th>Jam Mulai</th>
                            <th>Jam Selesai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jadwal as $i)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $i->nama_matkul }}</td>
                                <td>MI {{ $i->nama_kelas }}{{ $i->tingkat }} / {{ $i->smt }}</td>
                                <td>{{ $i->mulai }}</td>
                                <td>{{ $i->selesai }}</td>
                                <td>
                                    <a href="{{ route('dosen-jadwal.show', $i->id_jadwal) }}" class="btn btn-info"><i class="fas fa-info"></i>&ensp;Absen</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
