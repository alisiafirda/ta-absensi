@extends('layout.Admin')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List Absen</h3>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tr>
                    <td>Mata Kuliah</td>
                    <td>{{ $jadwal->nama_matkul }}</td>
                </tr>
                <tr>
                    <td>Kelas</td>
                    <td>MI {{ $jadwal->nama_kelas }}{{ $jadwal->tingkat }} / {{ $jadwal->smt }}</td>
                </tr>
                <tr>
                    <td>Jam Mulai</td>
                    <td>{{ $jadwal->mulai }}</td>
                </tr>
                <tr>
                    <td>Jam Selesai</td>
                    <td>{{ $jadwal->selesai }}</td>
                </tr>
            </table>
            <div class="table-responsive">
                <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th colspan="5" class="text-center">Data Absen</th>
                        </tr>
                        <tr>
                            <th>No.</th>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>Status</th>
                            <th>Ket.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($absen as $i)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $i->nim }}</td>
                                <td>{{ $i->nama_lengkap }}</td>
                                <td>{{ $i->st_absen }}</td>
                                <td>{{ $i->keterangan }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
