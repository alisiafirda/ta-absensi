@extends('layout.Mahasiswa')
@section('extracss')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap5/css/bootstrap.min.css') }}">
    <link href="{{ asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <style>
        .page-link {
            color: #fff;
            pointer-events: none;
            background-color: #575757;
            border-color: #575747;
        }

    </style>
@endsection

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="container mx-5">
        <header class="major">
            <h3>DATA DOSEN MAHASISWA MANAJEMEN INFORMATIKA</h3>
        </header>
        <div class="table-wrapper row4">
            <table class="alt datatable">
                <thead>
                    <tr>
                        <th>NIDN</th>
                        <th>Nama Dosen</th>
                        <th>Jenis Kelamin</th>
                        <th>No. HP</th>
                        <th>Mata Kuliah</th>
                        <th>Semester</th>
                        <th>Tingkat</th>
                        <th>Kelas</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dosen as $item)
                        <tr>
                            <td>{{ $item->nidn }}</td>
                            <td>{{ $item->nama_dosen }}</td>
                            <td>{{ $item->jenis_kelamin }}</td>
                            <td>{{ $item->hp }}</td>
                            <td class="text-center">
                                @php
                                    $jam = date('H:i:s');
                                    $jadwal = \App\Models\Jadwal::select('jadwal.*')
                                        ->join('jwd_jm AS jw', 'jw.id_jadwal', 'jadwal.id_jadwal')
                                        ->join('jam AS j', function ($j) use ($jam) {
                                            $j->on('jw.id_jam', 'j.id_jam')
                                                ->where('j.mulai', '<=', $jam)
                                                ->where('j.selesai', '>=', $jam);
                                        })
                                        ->where('jadwal.nidn', '=', $item->nidn)
                                        ->whereRaw('DAYOFWEEK(CURDATE()) = int_hari')
                                        ->first();
                                    // if (isset($jadwal->jam_mulai) && isset($jadwal->jam_selesai)) {
                                    //     $start = $jadwal->jam_mulai;
                                    //     $end = $jadwal->jam_selesai;
                                    // }
                                    if (isset($jadwal->kode_matkul)) {
                                        $matkul = \App\Models\Matkul::select('matkul.*')
                                            ->where('matkul.kode_matkul', '=', $jadwal->kode_matkul)
                                            ->first();
                                    } else {
                                        $matkul = null;
                                    }
                                @endphp
                                @if (isset($matkul->nama_matkul))
                                    <span class="badge bg-success fs-6 fw-bold">
                                        {{ $matkul->nama_matkul }}
                                    </span>
                                @else
                                    <span class="badge bg-danger fs-6 fw-bold">
                                        Kosong
                                    </span>
                                @endif
                            </td>
                            <td>{{ $item->semester }}</td>
                            <td>{{ $item->tingkat }}</td>
                            <td>{{ $item->nama_kelas }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- $dosen->links() --}}
        </div>
    </div>
@endsection

@section('extrajs')
    <script src="{{ asset('admin/assets/extra-libs/multicheck/datatable-checkbox-init.js') }}"></script>
    <script src="{{ asset('admin/assets/extra-libs/multicheck/jquery.multicheck.js') }}"></script>
    <script src="{{ asset('admin/assets/extra-libs/DataTables/datatables.min.js') }}"></script>

    <script>
        $(function() {
            $('.datatable').DataTable({
                dom: '<"table-wrapper row4"t<"row"<"col-sm-12 col-md-5"i><"col-sm-12 col-md-7" p>>'
            });
        })

    </script>
@endsection
