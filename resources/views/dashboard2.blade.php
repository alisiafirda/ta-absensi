@extends('layout.Mahasiswa')
@section('content')
    <!-- Banner -->
    <section id="banner">
        <div class="content">
            <header class="major">
                <h2>Welcome {{ auth()->user()->name }} !</h2>
                <p>SISTEM INFORMASI ABSENSI MAHASISWA MANAJEMEN INFORMATIKA<br />
                    PSDKU POLITEKNIK NEGERI MALANG DI KOTA KEDIRI</p>
            </header>
            <span class="image"><img sizes="10x10" src="/dua/images/logo.png" alt="" /></span>
        </div>
    </section>
    
@endsection
