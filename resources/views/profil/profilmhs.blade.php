@extends('layout.Mahasiswa')
@section('content')
    @if ($msg = session()->get('success'))
        <script>
            alert("{{ $msg }}")
        </script>
    @endif
    <div class="container mx-5">
        <header class="major">
            <h3>PROFIL MAHASISWA MANAJEMEN INFORMATIKA</h3>
        </header>
        <form action="{{ route('mhs.profil.ubahPass') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped" id="dataTable">
                        <tr>
                            <td><i class="fas fa-fw fa-star"></i> NIM</td>
                            <td>{{ $mahasiswa->nim }}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-fw fa-id-card"></i> Nama Mahasiswa</td>
                            <td>{{ $mahasiswa->nama_mhs }}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-fw fa-book"></i> Program Studi</td>
                            <td>{{ $mahasiswa->prodi }}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-fw fa-chart-bar"></i> Semester</td>
                            <td>{{ $mahasiswa->semester }}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-fw fa-calendar-alt"></i> Tempat, Tanggal Lahir</td>
                            <td>{{ $mahasiswa->ttl }}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-fw fa-venus-mars"></i> Jenis Kelamin</td>
                            <td>{{ $mahasiswa->jenis_kelamin }}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-fw fa-home"></i> Alamat</td>
                            <td>{{ $mahasiswa->alamat_lengkap }}</td>
                        </tr>
                        <tr>
                            <td><i class="fas fa-fw fa-calendar-check"></i> Agama</td>
                            <td>{{ $mahasiswa->agama }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-5">
                    <div class="form-group row">
                        <label for="">Ubah Password</label>
                        <div class="col-10">
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="col-2">
                            <input type="submit" value="Ubah">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
