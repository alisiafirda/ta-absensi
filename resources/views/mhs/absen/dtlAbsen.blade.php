@extends('layout.Mahasiswa')
@section('content')
    @if ($msg = session()->get('status'))
        <script>
            alert("{{ $msg }}")

        </script>
    @endif
    <div class="container mx-5">
        <header class="major">
            <h3>DETAIL ABSENSI</h3>
        </header>
        <!-- Form -->
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="text-align: center" colspan="3">KOMULATIF ABSENSI</th>
                    <th rowspan="2" style="text-align: center; vertical-align: middle">AIS</th>
                    <th rowspan="2" style="text-align: center; vertical-align: middle">KET.</th>
                </tr>
                <tr>
                    <th style="text-align: center">A</th>
                    <th style="text-align: center">I</th>
                    <th style="text-align: center">S</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="text-align: center">{{ $ais[0]->alpha }}</td>
                    <td style="text-align: center">{{ $ais[0]->ijin }}</td>
                    <td style="text-align: center">{{ $ais[0]->sakit }}</td>
                    <td style="text-align: center">{{ $ais[0]->total }}</td>
                    <td style="text-align: center">{{ $ket }}</td>
                </tr>
            </tbody>
        </table>
        <header class="major">
            <h3>RIWAYAT ABSENSI</h3>
        </header>
        <table class="table table-bordered">
            <thead>
                <th>Mata Kuliah</th>
                <th>Tanggal</th>
                <th>Status</th>
                <th>Keterangan</th>
            </thead>
            <tbody>
                @if (count($absen) === 0)
                    <tr>
                        <td colspan="4" align="center">Tidak Ada Absen</td>
                    </tr>
                @else
                    @foreach ($absen as $i)
                        @php
                            switch ($i->st_absen) {
                                case 'H':
                                    $st = 'Hadir';
                                    break;
                                case 'A':
                                    $st = 'Alpha';
                                    break;
                                case 'I':
                                    $st = 'Izin';
                                    break;
                                case 'S':
                                    $st = 'Sakit';
                                    break;
                                case 'WS':
                                    $st = 'Waiting';
                                    break;
                                case 'WI':
                                    $st = 'Waiting';
                                    break;
                                default:
                                    $st = '-';
                                    break;
                            }
                        @endphp
                        <tr>
                            <td>{{ $i->matkul }}</td>
                            <td>{{ $i->tanggal }}</td>
                            <td>{{ $st }}</td>
                            <td>{{ $i->keterangan }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
@endsection
