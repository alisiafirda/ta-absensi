<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie-edge">
    <title>Rekap Absensi</title>
    <style>
        #tabelrekap{
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 80%;
        }
        #tabelrekap td,#tabelrekap th{
            font-size: 13px;
            border: 1px solid #ddd;
            padding: 5px;
        }
        #tabelrekap th{
            font-size: 13px;
            padding-top: 8px;
            padding-buttom: 12px;
            text-align: center;
            background-color: #6495ED;
        }
    </style>
</head>
<table align="center">
    <tr>
    <td><img src="{{ public_path('admin/assets/images/LogoPolinema.png')}}" width="100" height="100"></td>
    <td></td>
    <td></td>
    <td><center>
        <font size="3">KEMENTRIAN PENDIDIKAN DAN KEBUDAYAAN</font><br>
        <font size="4"><b>POLITEKNIK NEGERI MALANG</b></font><br>
        <font size="3"><b>PSDKU KEDIRI</b></font><br>
        <font size="2">Kampus 1 : Jl. Mayor Bismo No. 27 Kota Kediri</font><br>
        <font size="2">Kampus 2 : Jl. Lingkar Maskumambang Kota Kediri</font><br>
        <font size="2">Telp. (0354) 683128 - Fax. (0354) 683128</font><br>
        <font size="2">Laman://www.polinema.ac.id - E-mail: info@polinema.ac.id</font><br>
        </center>
    </td>
    <td></td>
    <td></td>
    <td><img src="{{ public_path('admin/assets/images/LogoPolinema2.png')}}" width="100" height="100"></td>
    </tr>
    <tr>
        <td colspan="10"><hr></td>
    </tr>
</table>
<table align="center">
    <tr>
        <td><center>
            <font size="3"><b>REKAP ABSENSI MAHASISWA MANAJEMEN INFORMATIKA</b></font><br>
        </td>
    </tr>
</table>
<br>
<body>
    <table id="tabelrekap" align="center">
        <thead>
            <tr>
                <th>No</th>
                <th>NIM</th>
                <th>Nama Mahasiswa</th>
                <th>Tanggal</th>
                <th>Matkul</th>
                <th>Keterangan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($absensi as $item)
                <tr>
                    <td>{{ $item->no }}</td>
                    <td>{{ $item->nim }}</td>
                    <td>{{ $item->nama_lengkap }}</td>
                    <td>{{ $item->tanggal }}</td>
                    <td>{{ $item->matkul }}</td>
                    <td>{{ $item->keterangan }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
<br><br>
<table align="right">
    <tr>
        <td width="200">Kediri,</td>
    </tr>
</table>
<br><br><br><br>
<table align="right">

</table>
<br><br><br>
